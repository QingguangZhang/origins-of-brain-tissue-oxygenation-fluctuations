function plot_fig_5b(rootfolder)
data = load(fullfile(rootfolder, 'Data','Oxygen_Ephys_resp','QZ0371.mat'));  

seg.pre = [1020, 1270];
seg.post = [1800, 2050];

speed = data.speed;
oxygen = data.oxygen;
resp_rate = data.resp_rate;
Powergain_gamma = data.Powergain_gamma;
seg_idx_spectrogram = data.seg_idx_spectrogram;
spectrogram = data.spectrogram;
time.pre = data.time.pre;
time.post = data.time.post;

seg_idx.pre = (time.pre>= seg.pre(1)) & (time.pre <= seg.pre(2));
seg_idx.post = (time.post >= seg.post(1)) & (time.post <= seg.post(2));

%% generate figure
monitorPos = get(0,'MonitorPositions'); % [x y w ht] X # of monitors
w = 1000; ht = 300; % Define window size: width(w) and height(ht)
mon = 1; % Only one monitor

% Determine positioning for main figure
pos = [((monitorPos(mon,3)-w)/2)+monitorPos(mon,1)/2,...
    ((monitorPos(mon,4)-ht)/2)+monitorPos(mon,2)/2,...
    w ht];

% turn off warning to prevent MATLAB throwing warning
warning('off','MATLAB:legend:PlotEmpty');
warning('off','MATLAB:legend:IgnoringExtraEntries');

figure('name','Oxygen Tension Change during Locomotion: aCSF vs. CNQX/AP5/muscimol',...
    'position',pos,'color','w');

% axes for binarized speed data
h(11) = axes('units','normalized','position',[0.05 0.92 0.35 0.02],...
    'box','off','xtick',[],'ytick',[]);
hold on;
stem(h(11), time.pre(seg_idx.pre), speed.pre,...
    'marker','none','color','k'); % black impulse marker
set(gca,'XTickLabel','','YTickLabel','');
title('aCSF, probe depth = 170 \mum');
xlim(seg.pre);

% axes for oxygen data
h(21) = axes('units','normalized',...
    'position',[0.05 0.51 0.35 0.4],...
    'box','on');
hold on;
[H, L1, L2]= plotyy(time.pre(seg_idx.pre), oxygen.pre,...
                  time.pre(seg_idx.pre), resp_rate.pre);
set(H(1), 'xlim',seg.pre,'ylim',[10 50],'ytick',10:10:50);
set(L1,'color','k');
set(H(2),'xlim',seg.pre,'ylim',[0 10], 'ytick', 0:2:10);
set(L2,'color','r');
ylabel(H(2),'Resp rate (Hz)'); 
ylabel(h(21),'PtO_2 (mmHg)');
set(gca,'XTickLabel','');


% axes for spectrogram data
h(31) = axes('units','normalized',...
    'position',[0.05 0.09 0.35 0.4],...
    'box','on');
[H,~,~] = plotyy([],[],[],[],...
    @(x,y)imagesc(spectrogram.pre.time', spectrogram.pre.freq, log10(spectrogram.pre.power')),...
    @(x,y)plot(time.pre(seg_idx.pre), log10(Powergain_gamma.pre), 'color','c'));
caxis([-8 -3]);
colormap hot;
cb = colorbar('position',[0.45 0.09 0.01 0.4]);
set(get(cb,'title'),'string','Power (a.u.)');
set(cb, 'YTick',-8:1:-3,...
    'YTickLabel',{'10^{-8}','10^{-7}','10^{-6}','10^{-5}','10^{-4}','10^{-3}'});
set(H(1), 'YDir','normal','xlim',seg.pre,'ylim',[0 150],'ytick',0:50:150);
set(H(2),'xlim',seg.pre,'ylim',[-6 -3],'ytick', -6:1:-3);
ylabel(H(2),'Gamma-power')
xlabel('Time (second)');
ylabel('Frequency (Hz)');
linkaxes([h(11), h(21), h(31)],'x');


% axes for binarized speed data
h(12) = axes('units','normalized',...
    'position',[0.55 0.92 0.35 0.02],...
    'box','off',...
    'xtick',[],'ytick',[]);
hold on;
stem(h(12), time.post(seg_idx.post), speed.post,...
    'marker','none',...
    'color','k'); % black impulse marker
set(gca,'XTickLabel','','YTickLabel','');
title('CNQX/AP5/muscimol, probe depth = 170 \mum');
xlim(seg.post);

% axes for oxygen data
h(22) = axes('units','normalized',...
    'position',[0.55 0.51 0.35 0.4],...
    'box','on');
hold on;
[H, L1, L2]= plotyy(time.post(seg_idx.post), oxygen.post,...
                  time.post(seg_idx.post), resp_rate.post);
set(H(1),'xlim',seg.post, 'ylim',[10 50],'ytick',10:10:50);
set(L1,'color','k');
set(H(2),'xlim',seg.post, 'ylim',[0 10], 'ytick', 0:2:10);
set(L2,'color','r');
ylabel(H(2),'Resp rate (Hz)');             
ylabel(h(22),'PtO_2 (mmHg)');
set(gca,'XTickLabel','');

% axes for spectrogram data
h(32) = axes('units','normalized',...
    'position',[0.55 0.09 0.35 0.4],...
    'box','on');
[H,~,~] = plotyy([],[],[],[],...
    @(x,y)imagesc(spectrogram.post.time', spectrogram.post.freq, log10(spectrogram.post.power')),...
    @(x,y)plot(time.post(seg_idx.post), log10(Powergain_gamma.post), 'color','c'));
caxis([-8 -3]);
colormap hot;
cb = colorbar('position',[0.95 0.09 0.01 0.4]);
set(get(cb,'title'),'string','Power (a.u.)');
set(cb, 'YTick',-8:1:-3,...
    'YTickLabel',{'10^{-8}','10^{-7}','10^{-6}','10^{-5}','10^{-4}','10^{-3}'});
set(H(1), 'YDir','normal','xlim',seg.post, 'ylim',[0 150],'ytick',0:50:150);
set(H(2),'xlim',seg.post, 'ylim',[-6 -3], 'ytick', -6:1:-3);
ylabel(H(2),'Gamma-power')
xlabel('Time (second)');
ylabel('Frequency (Hz)');
linkaxes([h(12), h(22), h(32)],'x');


savefig(gcf, fullfile(rootfolder, 'Figure', 'Fig_5b'));
close(gcf);

end