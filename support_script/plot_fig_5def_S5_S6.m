function plot_fig_5def_S5_S6(rootfolder)
data = load(fullfile(rootfolder, 'Data','PowerLaw_drug.mat'));

probeLoc = data.probeLoc;
power_O2 = data.power_O2;
power_resp = data.power_resp;
power_Gamma = data.power_Gamma;
power_LFP = data.power_LFP;
predict_O2 = data.predict_O2;
predict_resp = data.predict_resp;
predict_Gamma = data.predict_Gamma;
predict_LFP = data.predict_LFP;
slope_O2 = data.slope_O2;
slope_LFP = data.slope_LFP;
slope_LFP_LF = data.slope_LFP_LF;
slope_Gamma = data.slope_Gamma;
slope_resp = data.slope_resp;
DFA_O2 = data.DFA_O2;
DFA_resp = data.DFA_resp;
DFA_Gamma = data.DFA_Gamma;
freq = data.freq;


freq_resamp.oxy = logspace(log10(freq.oxy(1)), log10(freq.oxy(end)), length(freq.oxy));
freq_resamp.LFP = logspace(log10(freq.LFP(263)), log10(freq.LFP(end)), length(freq.LFP(263:end)));

FC = find(~cellfun(@isempty, regexp(probeLoc, 'FC'))); % animals with oxygen measurement in FC
PC = find(~cellfun(@isempty, regexp(probeLoc, 'PC'))); % animals with oxygen measurement in PC


%% Normalized power and fitting
power_O2_norm.pre.rest = power_O2.pre.rest./sum(power_O2.pre.rest,2);
power_O2_norm.post.rest = power_O2.post.rest./sum(power_O2.post.rest,2);
power_O2_norm.pre.run = power_O2.pre.run./sum(power_O2.pre.run,2);
power_O2_norm.post.run = power_O2.post.run./sum(power_O2.post.run,2);

power_resp_norm.pre.rest = power_resp.pre.rest./sum(power_resp.pre.rest,2);
power_resp_norm.post.rest = power_resp.post.rest./sum(power_resp.post.rest,2);
power_resp_norm.pre.run = power_resp.pre.run./sum(power_resp.pre.run,2);
power_resp_norm.post.run = power_resp.post.run./sum(power_resp.post.run,2);

power_Gamma_norm.pre.rest = power_Gamma.pre.rest./sum(power_Gamma.pre.rest,2);
power_Gamma_norm.post.rest = power_Gamma.post.rest./sum(power_Gamma.post.rest,2);
power_Gamma_norm.pre.run = power_Gamma.pre.run./sum(power_Gamma.pre.run,2);
power_Gamma_norm.post.run = power_Gamma.post.run./sum(power_Gamma.post.run,2);

power_LFP_norm.pre.rest = power_LFP.pre.rest./sum(power_LFP.pre.rest,2);
power_LFP_norm.post.rest = power_LFP.post.rest./sum(power_LFP.post.rest,2);
power_LFP_norm.pre.run = power_LFP.pre.run./sum(power_LFP.pre.run,2);
power_LFP_norm.post.run = power_LFP.post.run./sum(power_LFP.post.run,2);

predict_O2_norm.pre.rest = log10(10.^(predict_O2.pre.rest)./sum(power_O2.pre.rest,2));
predict_O2_norm.post.rest = log10(10.^(predict_O2.post.rest)./sum(power_O2.post.rest,2));
predict_O2_norm.pre.run = log10(10.^(predict_O2.pre.run)./sum(power_O2.pre.run,2));
predict_O2_norm.post.run = log10(10.^(predict_O2.post.run)./sum(power_O2.post.run,2));

predict_resp_norm.pre.rest = log10(10.^(predict_resp.pre.rest)./sum(power_resp.pre.rest,2));
predict_resp_norm.post.rest = log10(10.^(predict_resp.post.rest)./sum(power_resp.post.rest,2));
predict_resp_norm.pre.run = log10(10.^(predict_resp.pre.run)./sum(power_resp.pre.run,2));
predict_resp_norm.post.run = log10(10.^(predict_resp.post.run)./sum(power_resp.post.run,2));

predict_LFP_norm.pre.rest = log10(10.^(predict_LFP.pre.rest)./sum(power_LFP.pre.rest,2));
predict_LFP_norm.post.rest = log10(10.^(predict_LFP.post.rest)./sum(power_LFP.post.rest,2));
predict_LFP_norm.pre.run = log10(10.^(predict_LFP.pre.run)./sum(power_LFP.pre.run,2));
predict_LFP_norm.post.run = log10(10.^(predict_LFP.post.run)./sum(power_LFP.post.run,2));

predict_Gamma_norm.pre.rest = log10(10.^(predict_Gamma.pre.rest)./sum(power_Gamma.pre.rest,2));
predict_Gamma_norm.post.rest = log10(10.^(predict_Gamma.post.rest)./sum(power_Gamma.post.rest,2));
predict_Gamma_norm.pre.run = log10(10.^(predict_Gamma.pre.run)./sum(power_Gamma.pre.run,2));
predict_Gamma_norm.post.run = log10(10.^(predict_Gamma.post.run)./sum(power_Gamma.post.run,2));

%% Oxygen, absolute values
figure('position', [100 100 1200 600],'color','w','name','Oxygen PSD before and after CNQX/AP5/muscimol');
h1 = subplot(121);
pt1 = box_line(freq.oxy,nanmean(power_O2.pre.rest,1),nanstd(power_O2.pre.rest,[],1)/sqrt(8),'k');
hold on;
pt2 = box_line(freq.oxy,nanmean(power_O2.post.rest,1),nanstd(power_O2.post.rest,[],1)/sqrt(8),'r');
plot(freq.oxy,nanmean(power_O2.pre.rest,1),'k');
plot(freq.oxy,nanmean(power_O2.post.rest,1),'r');
plot(freq_resamp.oxy(:), 10.^(nanmean(predict_O2.pre.rest)),'color','k','linestyle','--');
plot(freq_resamp.oxy(:), 10.^(nanmean(predict_O2.post.rest)),'color','r','linestyle','--');
set(gca,'box','off','xscale','log','yscale','log');
axis square;
axis([0.01 1 1e-3 1e3]);
text(0.1,0.15,...
    ['aCSF: log10(Power) = ', num2str(nanmean(slope_O2.pre.rest)), '* log10(f)'],...
    'unit','normalized','color','k');
text(0.1,0.2,...
    ['CNQX/AP5/Muscimol: log10(Power) = ', num2str(nanmean(slope_O2.post.rest)), '* log10(f)'],...
    'unit','normalized','color','r');
title('Oxygen power spectrum: rest');
xlabel('Frequency (Hz)');
ylabel('Oxygen power (mmHg^2/Hz)');

h2 = subplot(122);
pt1 = box_line(freq.oxy,nanmean(power_O2.pre.run,1),nanstd(power_O2.pre.run,[],1)/sqrt(9),'k');
hold on;
pt2 = box_line(freq.oxy,nanmean(power_O2.post.run,1),nanstd(power_O2.post.run,[],1)/sqrt(9),'r');
plot(freq.oxy,nanmean(power_O2.pre.run,1),'k');
plot(freq.oxy,nanmean(power_O2.post.run,1),'r');
plot(freq_resamp.oxy(:), 10.^(nanmean(predict_O2.pre.run)),'color','k','linestyle','--');
plot(freq_resamp.oxy(:), 10.^(nanmean(predict_O2.post.run)),'color','r','linestyle','--');
set(gca,'box','off','xscale','log','yscale','log');
axis square;
axis([0.01 1 1e-3 1e3]);
text(0.1,0.15,...
    ['aCSF: log10(Power) = ', num2str(nanmean(slope_O2.pre.run)), '* log10(f)'],...
    'unit','normalized','color','k');
text(0.1,0.2,...
    ['CNQX/AP5/Muscimol: log10(Power) = ', num2str(nanmean(slope_O2.post.run)), '* log10(f)'],...
    'unit','normalized','color','r');
title('Oxygen power spectrum: all data');
xlabel('Frequency (Hz)');
ylabel('Oxygen power (mmHg^2/Hz)');
savefig(gcf, fullfile(rootfolder, 'Figure', 'Fig_5d'));
close(gcf);

%% check oxygen power change pre vs post CNQX/AP5/muscimol
LF_seg = (freq.oxy<=0.1);
HF_seg = (freq.oxy>0.1);

% >>> LF segment, rest
LF_power.pre.rest = nanmean(power_O2.pre.rest(:,LF_seg),2);
LF_power.post.rest = nanmean(power_O2.post.rest(:,LF_seg),2);

% nanmean(LF_power.pre.rest)
% nanstd(LF_power.pre.rest)
% 
% nanmean(LF_power.post.rest)
% nanstd(LF_power.post.rest)

tmp = [LF_power.pre.rest, LF_power.post.rest];
% first, check if the data is normalized
normal_distribution = adtest(tmp(:)); % 1, non-normal
% second , check if the data is equal variance
equal_variance = vartest2(tmp(:,1), tmp(:,2)); % 0, equal variance
[p,h,stats] = ranksum(tmp(:,1), tmp(:,2));

% >>> HF segment, rest
HF_power.pre.rest = nanmean(power_O2.pre.rest(:,HF_seg),2);
HF_power.post.rest = nanmean(power_O2.post.rest(:,HF_seg),2);

% nanmean(HF_power.pre.rest)
% nanstd(HF_power.pre.rest)
% 
% nanmean(HF_power.post.rest)
% nanstd(HF_power.post.rest)

tmp = [HF_power.pre.rest, HF_power.post.rest];
% first, check if the data is normalized
normal_distribution = adtest(tmp(:)); % 1, non-normal
% second , check if the data is equal variance
equal_variance = vartest2(tmp(:,1), tmp(:,2)); % 0, equal variance
[p,h,stats] = ranksum(tmp(:,1), tmp(:,2));

%% Bar plot of power law exponent: oxygen, Median +/- IQR
x1 = [1 2];
figure('name','Power-law exponent before and after CNQX/AP5/muscimol infusion',...
    'position',[500 500 1200 400]);
h1 = subplot(161);
plot(x1,-[slope_O2.pre.rest(PC), slope_O2.post.rest(PC)],...
    'marker','o','markersize',10,...
    'markeredgecolor','k','markerfacecolor',[0.5 0.5 0.5],'linewidth',1,...
    'color',[0.5 0.5 0.5]); % data points in FL/HL
hold on;
plot(x1,-[slope_O2.pre.rest(FC), slope_O2.post.rest(FC)],...
    'marker','o','markersize',10,...
    'markeredgecolor',[0.91 0.41 0.17], 'markerfacecolor',[0.5 0.5 0.5],'linewidth',1,...
    'color',[0.5 0.5 0.5]); % data points in FC
boxplot(-[slope_O2.pre.rest,slope_O2.post.rest],x1,'Width',0.6,'Color','k','symbol','');
set(findobj(gcf,'-regexp','Tag','\w*Whisker'),'LineStyle','-');
hold on;
plot([x1(1)-0.3,x1(1)+0.3], nanmean(-slope_O2.pre.rest(:))*ones(1,2),'k--'); % plot mean as an extra
plot([x1(2)-0.3,x1(2)+0.3], nanmean(-slope_O2.post.rest(:))*ones(1,2),'k--');
axis([0.4 2.6 0 3]);
set(gca,'box','off','xtick',[],'xticklabel',[]);
ylabel('Power-law exponent, oxygen, rest');

h2 = subplot(162);
plot(x1,-[slope_O2.pre.run(PC), slope_O2.post.run(PC)],...
    'marker','o','markersize',10,...
    'markeredgecolor','k','markerfacecolor',[0.5 0.5 0.5],'linewidth',1,...
    'color',[0.5 0.5 0.5]); % data points in FL/HL
hold on;
plot(x1,-[slope_O2.pre.run(FC), slope_O2.post.run(FC)],...
    'marker','o','markersize',10,...
    'markeredgecolor',[0.91 0.41 0.17], 'markerfacecolor',[0.5 0.5 0.5],'linewidth',1,...
    'color',[0.5 0.5 0.5]); % data points in FC
boxplot(-[slope_O2.pre.run,slope_O2.post.run],x1,'Width',0.6,'Color','k','symbol','');
set(findobj(gcf,'-regexp','Tag','\w*Whisker'),'LineStyle','-');
hold on;
plot([x1(1)-0.3,x1(1)+0.3], nanmean(-slope_O2.pre.run(:))*ones(1,2),'k--'); % plot mean as an extra
plot([x1(2)-0.3,x1(2)+0.3], nanmean(-slope_O2.post.run(:))*ones(1,2),'k--');
axis([0.4 2.6 0 3]);
set(gca,'box','off','xtick',[],'xticklabel',[]);
ylabel('Power-law exponent, oxygen, all data');

savefig(gcf, fullfile(rootfolder, 'Figure', 'Fig_5e'));
close(gcf);

%% Statistics on Oxygen Power law slope: aCSF vs. CNQX/AP5/muscimol
% >>> rest
tmp = [-slope_O2.pre.rest, -slope_O2.post.rest];

% nanmean(slope_O2.pre.rest)
% nanstd(slope_O2.pre.rest)
% 
% nanmean(slope_O2.post.rest)
% nanstd(slope_O2.post.rest)

% first, check if the data is normalized
normal_distribution = adtest(tmp(:)); % 0, normal
% second , check if the data is equal variance
equal_variance = vartest2(tmp(:,1), tmp(:,2)); % 0, equal variance
% since dataset meets the normal distribution and equal variance, use
% paired t-test
[~, p, ci, stats] = ttest(tmp(:,1), tmp(:,2)); % p = 0.0013, t(7) = 5.1790

% >>> rest and run
tmp = [-slope_O2.pre.run, -slope_O2.post.run];

% nanmean(slope_O2.pre.run)
% nanstd(slope_O2.pre.run)
% 
% nanmean(slope_O2.post.run)
% nanstd(slope_O2.post.run)

% first, check if the data is normalized
normal_distribution = adtest(tmp(:)); % 0, normal
% second , check if the data is equal variance
equal_variance = vartest2(tmp(:,1), tmp(:,2)); % 0, equal variance
% since dataset meets the normal distribution and equal variance, use
% paired t-test
[~, p, ci, stats] = ttest(tmp(:,1), tmp(:,2)); % p = 0.0318, t(8) = 2.5967

%% Broad-band LFP power before and after CNQX/AP5/muscimol: normalized
figure('position', [100 100 1200 600],'color','w','name','LFP PSD before and after CNQX/AP5/muscimol');
h1 = subplot(121);
pt1 = box_line(freq.LFP,nanmean(power_LFP_norm.pre.rest,1),nanstd(power_LFP_norm.pre.rest,[],1)/sqrt(8),'k');
hold on;
pt2 = box_line(freq.LFP,nanmean(power_LFP_norm.post.rest,1),nanstd(power_LFP_norm.post.rest,[],1)/sqrt(8),'r');
plot(freq.LFP,nanmean(power_LFP_norm.pre.rest,1),'k');
plot(freq.LFP,nanmean(power_LFP_norm.post.rest,1),'r');
plot(freq_resamp.LFP(:), 10.^(nanmean(predict_LFP_norm.pre.rest)),'color','k','linestyle','--');
plot(freq_resamp.LFP(:), 10.^(nanmean(predict_LFP_norm.post.rest)),'color','r','linestyle','--');
set(gca,'box','off','xscale','log','yscale','log');
axis square;
axis([1 100 10^(-6.3) 10^(-2)]);
text(0.1,0.15,...
    ['aCSF: log10(Power) = ', num2str(nanmean(slope_LFP.pre.rest)), '* log10(f)'],...
    'unit','normalized','color','k');
text(0.1,0.2,...
    ['CNQX/AP5/Muscimol: log10(Power) = ', num2str(nanmean(slope_LFP.post.rest)), '* log10(f)'],...
    'unit','normalized','color','r');
title('LFP power spectrum: rest');
xlabel('Frequency (Hz)');
ylabel('LFP power (au)');

h2 = subplot(122);
pt1 = box_line(freq.LFP,nanmean(power_LFP_norm.pre.run,1),nanstd(power_LFP_norm.pre.run,[],1)/sqrt(9),'k');
hold on;
pt2 = box_line(freq.LFP,nanmean(power_LFP_norm.post.run,1),nanstd(power_LFP_norm.post.run,[],1)/sqrt(9),'r');
plot(freq.LFP,nanmean(power_LFP_norm.pre.run,1),'k');
plot(freq.LFP,nanmean(power_LFP_norm.post.run,1),'r');
plot(freq_resamp.LFP(:), 10.^(nanmean(predict_LFP_norm.pre.run)),'color','k','linestyle','--');
plot(freq_resamp.LFP(:), 10.^(nanmean(predict_LFP_norm.post.run)),'color','r','linestyle','--');
set(gca,'box','off','xscale','log','yscale','log');
axis square;
axis([1 100 10^(-6.3) 10^(-2)]);
text(0.1,0.15,...
    ['aCSF: log10(Power) = ', num2str(nanmean(slope_LFP.pre.run)), '* log10(f)'],...
    'unit','normalized','color','k');
text(0.1,0.2,...
    ['CNQX/AP5/Muscimol: log10(Power) = ', num2str(nanmean(slope_LFP.post.run)), '* log10(f)'],...
    'unit','normalized','color','r');
title('LFP power spectrum: run');
xlabel('Frequency (Hz)');
ylabel('LFP power (au)');
savefig(gcf, fullfile(rootfolder, 'Figure', 'Fig_S5b'));
close(gcf);

% >>> Stats, HF of LFP
% rest period
mean(-slope_LFP.pre.rest);
std(slope_LFP.pre.rest);

mean(-slope_LFP.post.rest);
std(slope_LFP.post.rest);

% first, check if the data is normalized
normal_distribution = adtest([slope_LFP.pre.rest; slope_LFP.post.rest;]); % 0, normal
% second , check if the data is equal variance
equal_variance = vartest2(slope_LFP.pre.rest, slope_LFP.post.rest); % 0, equal variance
[h,p,ci,stats] = ttest(slope_LFP.pre.rest, slope_LFP.post.rest); % P = 0.6035


% running period
% rest period
mean(-slope_LFP.pre.run);
std(slope_LFP.pre.run);

mean(-slope_LFP.post.run);
std(slope_LFP.post.run);

% first, check if the data is normalized
normal_distribution = adtest([slope_LFP.pre.run; slope_LFP.post.run;]); % 0, normal
% second , check if the data is equal variance
equal_variance = vartest2(slope_LFP.pre.run, slope_LFP.post.run); % 0, equal variance
[h,p,ci,stats] = ttest(slope_LFP.pre.run, slope_LFP.post.run); % P = 0.6035

% >>> Stats, LF of LFP
% rest period
% mean(-slope_LFP_LF.pre.rest)
% std(slope_LFP_LF.pre.rest)
% 
% mean(-slope_LFP_LF.post.rest)
% std(slope_LFP_LF.post.rest)

% first, check if the data is normalized
normal_distribution = adtest([slope_LFP_LF.pre.rest; slope_LFP_LF.post.rest;]); % 0, normal
% second , check if the data is equal variance
equal_variance = vartest2(slope_LFP_LF.pre.rest, slope_LFP_LF.post.rest); % 1, not equal variance
[p,h,stats] = ranksum(slope_LFP_LF.pre.rest, slope_LFP_LF.post.rest);


% running period
% rest period
% mean(-slope_LFP_LF.pre.run)
% std(slope_LFP_LF.pre.run)
% 
% mean(-slope_LFP_LF.post.run)
% std(slope_LFP_LF.post.run)

% first, check if the data is normalized
normal_distribution = adtest([slope_LFP_LF.pre.run; slope_LFP_LF.post.run;]); % 0, normal
% second , check if the data is equal variance
equal_variance = vartest2(slope_LFP_LF.pre.run, slope_LFP_LF.post.run); % 1, not equal variance
[p,h,stats] = ranksum(slope_LFP_LF.pre.run, slope_LFP_LF.post.run); % P = 0.6035


%% Gamma-band LFP
figure('position', [100 100 1200 600],'color','w','name','Gamma BLP PSD before and after CNQX/AP5/muscimol');
h1 = subplot(121);
pt1 = box_line(freq.oxy,nanmean(power_Gamma_norm.pre.rest,1),nanstd(power_Gamma_norm.pre.rest,[],1)/sqrt(8),'k');
hold on;
pt2 = box_line(freq.oxy,nanmean(power_Gamma_norm.post.rest,1),nanstd(power_Gamma_norm.post.rest,[],1)/sqrt(8),'r');
plot(freq.oxy,nanmean(power_Gamma_norm.pre.rest,1),'k');
plot(freq.oxy,nanmean(power_Gamma_norm.post.rest,1),'r');
plot(freq_resamp.oxy(:), 10.^(nanmean(predict_Gamma_norm.pre.rest)),'color','k','linestyle','--');
plot(freq_resamp.oxy(:), 10.^(nanmean(predict_Gamma_norm.post.rest)),'color','r','linestyle','--');
set(gca,'box','off','xscale','log','yscale','log');
axis square;
axis([0.01 1 10^(-2.7) 10^(-0.8)]);
text(0.1,0.15,...
    ['aCSF: log10(Power) = ', num2str(nanmean(slope_Gamma.pre.rest)), '* log10(f)'],...
    'unit','normalized','color','k');
text(0.1,0.2,...
    ['CNQX/AP5/Muscimol: log10(Power) = ', num2str(nanmean(slope_Gamma.post.rest)), '* log10(f)'],...
    'unit','normalized','color','r');
title('Gamma BLP power spectrum: rest');
xlabel('Frequency (Hz)');
ylabel('Gamma BLP power (au)');

h2 = subplot(122);
pt1 = box_line(freq.oxy,nanmean(power_Gamma_norm.pre.run,1),nanstd(power_Gamma_norm.pre.run,[],1)/sqrt(9),'k');
hold on;
pt2 = box_line(freq.oxy,nanmean(power_Gamma_norm.post.run,1),nanstd(power_Gamma_norm.post.run,[],1)/sqrt(9),'r');
plot(freq.oxy,nanmean(power_Gamma_norm.pre.run,1),'k');
plot(freq.oxy,nanmean(power_Gamma_norm.post.run,1),'r');
plot(freq_resamp.oxy(:), 10.^(nanmean(predict_Gamma_norm.pre.run)),'color','k','linestyle','--');
plot(freq_resamp.oxy(:), 10.^(nanmean(predict_Gamma_norm.post.run)),'color','r','linestyle','--');
set(gca,'box','off','xscale','log','yscale','log');
axis square;
axis([0.01 1 10^(-2.7) 10^(-0.8)]);
text(0.1,0.15,...
    ['aCSF: log10(Power) = ', num2str(nanmean(slope_Gamma.pre.run)), '* log10(f)'],...
    'unit','normalized','color','k');
text(0.1,0.2,...
    ['CNQX/AP5/Muscimol: log10(Power) = ', num2str(nanmean(slope_Gamma.post.run)), '* log10(f)'],...
    'unit','normalized','color','r');
title('Gamma BLP power spectrum: run');
xlabel('Frequency (Hz)');
ylabel('Gamma BLP power (au)');
savefig(gcf, fullfile(rootfolder, 'Figure', 'Fig_S5d'));
close(gcf);


%% Bar plot of power law exponent: broad-band LFP, median +/- IQR
x1 = [1 2];
figure('name','Power-law exponent before and after CNQX/AP5/muscimol infusion',...
    'position',[500 500 1200 400]);
h1 = subplot(161);
plot(x1,-[slope_LFP.pre.rest(PC), slope_LFP.post.rest(PC)],...
    'marker','o','markersize',10,...
    'markeredgecolor','k','markerfacecolor',[0.5 0.5 0.5],'linewidth',1,...
    'color',[0.5 0.5 0.5]); % data points in FL/HL
hold on;
plot(x1,-[slope_LFP.pre.rest(FC), slope_LFP.post.rest(FC)],...
    'marker','o','markersize',10,...
    'markeredgecolor',[0.91 0.41 0.17], 'markerfacecolor',[0.5 0.5 0.5],'linewidth',1,...
    'color',[0.5 0.5 0.5]); % data points in FC
boxplot(-[slope_LFP.pre.rest,slope_LFP.post.rest],x1,'Width',0.6,'Color','k','symbol','');
set(findobj(gcf,'-regexp','Tag','\w*Whisker'),'LineStyle','-');
hold on;
plot([x1(1)-0.3,x1(1)+0.3], nanmean(-slope_LFP.pre.rest(:))*ones(1,2),'k--'); % plot mean as an extra
plot([x1(2)-0.3,x1(2)+0.3], nanmean(-slope_LFP.post.rest(:))*ones(1,2),'k--');
axis([0.4 2.6 0 3]);
set(gca,'box','off','xtick',[],'xticklabel',[]);
ylabel('Power-law exponent, LFP, rest');

h2 = subplot(162);
plot(x1,-[slope_LFP.pre.run(PC), slope_LFP.post.run(PC)],...
    'marker','o','markersize',10,...
    'markeredgecolor','k','markerfacecolor',[0.5 0.5 0.5],'linewidth',1,...
    'color',[0.5 0.5 0.5]); % data points in FL/HL
hold on;
plot(x1,-[slope_LFP.pre.run(FC), slope_LFP.post.run(FC)],...
    'marker','o','markersize',10,...
    'markeredgecolor',[0.91 0.41 0.17], 'markerfacecolor',[0.5 0.5 0.5],'linewidth',1,...
    'color',[0.5 0.5 0.5]); % data points in FC
boxplot(-[slope_LFP.pre.run,slope_LFP.post.run],x1,'Width',0.6,'Color','k','symbol','');
set(findobj(gcf,'-regexp','Tag','\w*Whisker'),'LineStyle','-');
hold on;
plot([x1(1)-0.3,x1(1)+0.3], nanmean(-slope_LFP.pre.run(:))*ones(1,2),'k--'); % plot mean as an extra
plot([x1(2)-0.3,x1(2)+0.3], nanmean(-slope_LFP.post.run(:))*ones(1,2),'k--');
axis([0.4 2.6 0 3]);
set(gca,'box','off','xtick',[],'xticklabel',[]);
ylabel('Power-law exponent, LFP, all data');
savefig(gcf, fullfile(rootfolder, 'Figure', 'Fig_S5c'));
close(gcf);


%% Bar plot of power law exponent: gamma-band LFP, median +/- IQR
x1 = [1 2];
figure('name','Power-law exponent before and after CNQX/AP5/muscimol infusion',...
    'position',[500 500 1200 400]);
h1 = subplot(161);
plot(x1,-[slope_Gamma.pre.rest(PC), slope_Gamma.post.rest(PC)],...
    'marker','o','markersize',10,...
    'markeredgecolor','k','markerfacecolor',[0.5 0.5 0.5],'linewidth',1,...
    'color',[0.5 0.5 0.5]); % data points in FL/HL
hold on;
plot(x1,-[slope_Gamma.pre.rest(FC), slope_Gamma.post.rest(FC)],...
    'marker','o','markersize',10,...
    'markeredgecolor',[0.91 0.41 0.17], 'markerfacecolor',[0.5 0.5 0.5],'linewidth',1,...
    'color',[0.5 0.5 0.5]); % data points in FC
boxplot(-[slope_Gamma.pre.rest,slope_Gamma.post.rest],x1,'Width',0.6,'Color','k','symbol','');
set(findobj(gcf,'-regexp','Tag','\w*Whisker'),'LineStyle','-');
hold on;
plot([x1(1)-0.3,x1(1)+0.3], nanmean(-slope_Gamma.pre.rest(:))*ones(1,2),'k--'); % plot mean as an extra
plot([x1(2)-0.3,x1(2)+0.3], nanmean(-slope_Gamma.post.rest(:))*ones(1,2),'k--');
axis([0.4 2.6 -0.6 1]);
set(gca,'box','off','xtick',[],'xticklabel',[]);
ylabel('Power-law exponent, Gamma, rest');

h2 = subplot(162);
plot(x1,-[slope_Gamma.pre.run(PC), slope_Gamma.post.run(PC)],...
    'marker','o','markersize',10,...
    'markeredgecolor','k','markerfacecolor',[0.5 0.5 0.5],'linewidth',1,...
    'color',[0.5 0.5 0.5]); % data points in FL/HL
hold on;
plot(x1,-[slope_Gamma.pre.run(FC), slope_Gamma.post.run(FC)],...
    'marker','o','markersize',10,...
    'markeredgecolor',[0.91 0.41 0.17], 'markerfacecolor',[0.5 0.5 0.5],'linewidth',1,...
    'color',[0.5 0.5 0.5]); % data points in FC
boxplot(-[slope_Gamma.pre.run,slope_Gamma.post.run],x1,'Width',0.6,'Color','k','symbol','');
set(findobj(gcf,'-regexp','Tag','\w*Whisker'),'LineStyle','-');
hold on;
plot([x1(1)-0.3,x1(1)+0.3], nanmean(-slope_Gamma.pre.run(:))*ones(1,2),'k--'); % plot mean as an extra
plot([x1(2)-0.3,x1(2)+0.3], nanmean(-slope_Gamma.post.run(:))*ones(1,2),'k--');
axis([0.4 2.6 -0.6 1]);
set(gca,'box','off','xtick',[],'xticklabel',[]);
ylabel('Power-law exponent, Gamma, all data');
savefig(gcf, fullfile(rootfolder, 'Figure', 'Fig_S5e'));
close(gcf);



% >>> Stats
% >>> rest
tmp = [-slope_Gamma.pre.rest, -slope_Gamma.post.rest];

% nanmean(-slope_Gamma.pre.rest)
% nanstd(-slope_Gamma.pre.rest)
% 
% nanmean(-slope_Gamma.post.rest)
% nanstd(-slope_Gamma.post.rest)

% first, check if the data is normalized
normal_distribution = adtest(tmp(:)); % 0, normal
% second , check if the data is equal variance
equal_variance = vartest2(tmp(:,1), tmp(:,2)); % 0, equal variance
[p,h,stats] = ranksum(tmp(:,1), tmp(:,2));

% >>> rest and run
tmp = [-slope_Gamma.pre.run, -slope_Gamma.post.run];

% nanmean(-slope_Gamma.pre.run)
% nanstd(-slope_Gamma.pre.run)
% 
% nanmean(-slope_Gamma.post.run)
% nanstd(-slope_Gamma.post.run)

% first, check if the data is normalized
normal_distribution = adtest(tmp(:)); % 1, not normal
% second , check if the data is equal variance
equal_variance = vartest2(tmp(:,1), tmp(:,2)); % 0, equal variance
[p,h,stats] = ranksum(tmp(:,1), tmp(:,2));
%% Resp, absolute values
figure('position', [100 100 1200 600],'color','w','name','Resp PSD before and after CNQX/AP5/muscimol');
h1 = subplot(121);
pt1 = box_line(freq.oxy,nanmean(power_resp.pre.rest,1),nanstd(power_resp.pre.rest,[],1)/sqrt(8),'k');
hold on;
pt2 = box_line(freq.oxy,nanmean(power_resp.post.rest,1),nanstd(power_resp.post.rest,[],1)/sqrt(8),'r');
plot(freq.oxy,nanmean(power_resp.pre.rest,1),'k');
plot(freq.oxy,nanmean(power_resp.post.rest,1),'r');
plot(freq_resamp.oxy(:), 10.^(nanmean(predict_resp.pre.rest)),'color','k','linestyle','--');
plot(freq_resamp.oxy(:), 10.^(nanmean(predict_resp.post.rest)),'color','r','linestyle','--');
set(gca,'box','off','xscale','log','yscale','log');
axis square;
axis([0.01 1 1e-2 1e1]);
text(0.1,0.15,...
    ['aCSF: log10(Power) = ', num2str(nanmean(slope_resp.pre.rest)), '* log10(f)'],...
    'unit','normalized','color','k');
text(0.1,0.2,...
    ['CNQX/AP5/Muscimol: log10(Power) = ', num2str(nanmean(slope_resp.post.rest)), '* log10(f)'],...
    'unit','normalized','color','r');
title('Resp rate power spectrum: rest');
xlabel('Frequency (Hz)');
ylabel('Resp rate power (au)');

h2 = subplot(122);
pt1 = box_line(freq.oxy,nanmean(power_resp.pre.run,1),nanstd(power_resp.pre.run,[],1)/sqrt(9),'k');
hold on;
pt2 = box_line(freq.oxy,nanmean(power_resp.post.run,1),nanstd(power_resp.post.run,[],1)/sqrt(9),'r');
plot(freq.oxy,nanmean(power_resp.pre.run,1),'k');
plot(freq.oxy,nanmean(power_resp.post.run,1),'r');
plot(freq_resamp.oxy(:), 10.^(nanmean(predict_resp.pre.run)),'color','k','linestyle','--');
plot(freq_resamp.oxy(:), 10.^(nanmean(predict_resp.post.run)),'color','r','linestyle','--');
set(gca,'box','off','xscale','log','yscale','log');
axis square;
axis([0.01 1 1e-2 1e1]);
text(0.1,0.15,...
    ['aCSF: log10(Power) = ', num2str(nanmean(slope_resp.pre.run)), '* log10(f)'],...
    'unit','normalized','color','k');
text(0.1,0.2,...
    ['CNQX/AP5/Muscimol: log10(Power) = ', num2str(nanmean(slope_resp.post.run)), '* log10(f)'],...
    'unit','normalized','color','r');
title('Resp rate power spectrum: run');
xlabel('Frequency (Hz)');
ylabel('Resp rate power (au)');
savefig(gcf, fullfile(rootfolder, 'Figure', 'Fig_S6a'));
close(gcf);

%% Bar plot of power law exponent: resp rate, median +/- IQR
x1 = [1 2];
figure('name','Power-law exponent before and after CNQX/AP5/muscimol infusion',...
    'position',[500 500 1200 400]);
h1 = subplot(161);
plot(x1,-[slope_resp.pre.rest(PC), slope_resp.post.rest(PC)],...
    'marker','o','markersize',10,...
    'markeredgecolor','k','markerfacecolor',[0.5 0.5 0.5],'linewidth',1,...
    'color',[0.5 0.5 0.5]); % data points in FL/HL
hold on;
plot(x1,-[slope_resp.pre.rest(FC), slope_resp.post.rest(FC)],...
    'marker','o','markersize',10,...
    'markeredgecolor',[0.91 0.41 0.17], 'markerfacecolor',[0.5 0.5 0.5],'linewidth',1,...
    'color',[0.5 0.5 0.5]); % data points in FC
boxplot(-[slope_resp.pre.rest,slope_resp.post.rest],x1,'Width',0.6,'Color','k','symbol','');
set(findobj(gcf,'-regexp','Tag','\w*Whisker'),'LineStyle','-');
hold on;
plot([x1(1)-0.3,x1(1)+0.3], nanmean(-slope_resp.pre.rest(:))*ones(1,2),'k--'); % plot mean as an extra
plot([x1(2)-0.3,x1(2)+0.3], nanmean(-slope_resp.post.rest(:))*ones(1,2),'k--');
axis([0.4 2.6 0 0.9]);
set(gca,'box','off','xtick',[],'xticklabel',[]);
ylabel('Power-law exponent, Resp rate, rest');

h2 = subplot(162);
plot(x1,-[slope_resp.pre.run(PC), slope_resp.post.run(PC)],...
    'marker','o','markersize',10,...
    'markeredgecolor','k','markerfacecolor',[0.5 0.5 0.5],'linewidth',1,...
    'color',[0.5 0.5 0.5]); % data points in FL/HL
hold on;
plot(x1,-[slope_resp.pre.run(FC), slope_resp.post.run(FC)],...
    'marker','o','markersize',10,...
    'markeredgecolor',[0.91 0.41 0.17], 'markerfacecolor',[0.5 0.5 0.5],'linewidth',1,...
    'color',[0.5 0.5 0.5]); % data points in FC
plot(x1,-[slope_resp.pre.run(FC), slope_resp.post.run(FC)],...
    'marker','o','markersize',10,...
    'markeredgecolor',[0.91 0.41 0.17], 'markerfacecolor',[0.5 0.5 0.5],'linewidth',1,...
    'color',[0.5 0.5 0.5]); % data points in FC
boxplot(-[slope_resp.pre.run,slope_resp.post.run],x1,'Width',0.6,'Color','k','symbol','');
set(findobj(gcf,'-regexp','Tag','\w*Whisker'),'LineStyle','-');
hold on;
plot([x1(1)-0.3,x1(1)+0.3], nanmean(-slope_resp.pre.run(:))*ones(1,2),'k--'); % plot mean as an extra
plot([x1(2)-0.3,x1(2)+0.3], nanmean(-slope_resp.post.run(:))*ones(1,2),'k--');
axis([0.4 2.6 0 0.9]);
set(gca,'box','off','xtick',[],'xticklabel',[]);
ylabel('Power-law exponent, Resp rate, all data');
savefig(gcf, fullfile(rootfolder, 'Figure', 'Fig_S6b'));
close(gcf);

% >>> rest
tmp = [-slope_resp.pre.rest, -slope_resp.post.rest];

% nanmean(-slope_resp.pre.rest)
% nanstd(-slope_resp.pre.rest)
% 
% nanmean(-slope_resp.post.rest)
% nanstd(-slope_resp.post.rest)

% first, check if the data is normalized
normal_distribution = adtest(tmp(:)); % 0, normal
% second , check if the data is equal variance
equal_variance = vartest2(tmp(:,1), tmp(:,2)); % 1, not equal variance
[~, p, ci, stats] = ttest(tmp(:,1), tmp(:,2)); % t(8) = 1.5758, p = 0.1537

% >>> rest and run
tmp = [-slope_resp.pre.run, -slope_resp.post.run];

% nanmean(-slope_resp.pre.run)
% nanstd(-slope_resp.pre.run)
% 
% nanmean(-slope_resp.post.run)
% nanstd(-slope_resp.post.run)

% first, check if the data is normalized
normal_distribution = adtest(tmp(:)); % 1, not normal
% second , check if the data is equal variance
equal_variance = vartest2(tmp(:,1), tmp(:,2)); % 0, equal variance
[p,h,stats] = ranksum(tmp(:,1), tmp(:,2));


%% >> DFA plot
%% Bar plot of DFA exponent: oxygen, median +/- IQR
x1 = [1 2];
figure('name','DFA exponent before and after CNQX/AP5/muscimol infusion',...
    'position',[500 500 1200 400]);
h1 = subplot(161);
plot(x1,[DFA_O2.pre.rest(PC), DFA_O2.post.rest(PC)],...
    'marker','o','markersize',10,...
    'markeredgecolor','k','markerfacecolor',[0.5 0.5 0.5],'linewidth',1,...
    'color',[0.5 0.5 0.5]); % data points in FL/HL
hold on;
plot(x1,[DFA_O2.pre.rest(FC), DFA_O2.post.rest(FC)],...
    'marker','o','markersize',10,...
    'markeredgecolor',[0.91 0.41 0.17], 'markerfacecolor',[0.5 0.5 0.5],'linewidth',1,...
    'color',[0.5 0.5 0.5]); % data points in FC
boxplot([DFA_O2.pre.rest,DFA_O2.post.rest],x1,'Width',0.6,'Color','k','symbol','');
set(findobj(gcf,'-regexp','Tag','\w*Whisker'),'LineStyle','-');
hold on;
plot([x1(1)-0.3,x1(1)+0.3], nanmean(DFA_O2.pre.rest(:))*ones(1,2),'k--'); % plot mean as an extra
plot([x1(2)-0.3,x1(2)+0.3], nanmean(DFA_O2.post.rest(:))*ones(1,2),'k--');
axis([0.4 2.6 0 2]);
set(gca,'box','off','xtick',[],'xticklabel',[]);
ylabel('DFA exponent, oxygen, rest');

h2 = subplot(162);
plot(x1,[DFA_O2.pre.run(PC), DFA_O2.post.run(PC)],...
    'marker','o','markersize',10,...
    'markeredgecolor','k','markerfacecolor',[0.5 0.5 0.5],'linewidth',1,...
    'color',[0.5 0.5 0.5]); % data points in FL/HL
hold on;
plot(x1,[DFA_O2.pre.run(FC), DFA_O2.post.run(FC)],...
    'marker','o','markersize',10,...
    'markeredgecolor',[0.91 0.41 0.17], 'markerfacecolor',[0.5 0.5 0.5],'linewidth',1,...
    'color',[0.5 0.5 0.5]); % data points in FC
boxplot([DFA_O2.pre.run,DFA_O2.post.run],x1,'Width',0.6,'Color','k','symbol','');
set(findobj(gcf,'-regexp','Tag','\w*Whisker'),'LineStyle','-');
hold on;
plot([x1(1)-0.3,x1(1)+0.3], nanmean(DFA_O2.pre.run(:))*ones(1,2),'k--'); % plot mean as an extra
plot([x1(2)-0.3,x1(2)+0.3], nanmean(DFA_O2.post.run(:))*ones(1,2),'k--');
axis([0.4 2.6 0 2]);
set(gca,'box','off','xtick',[],'xticklabel',[]);
ylabel('DFA exponent, oxygen, all data');

savefig(gcf, fullfile(rootfolder, 'Figure', 'Fig_5f'));
close(gcf);

%% Statistics on Oxygen DFA slope: aCSF vs. CNQX/AP5/muscimol
% >>> rest
tmp = [DFA_O2.pre.rest, DFA_O2.post.rest];

% nanmean(DFA_O2.pre.rest)
% nanstd(DFA_O2.pre.rest)
% 
% nanmean(DFA_O2.post.rest)
% nanstd(DFA_O2.post.rest)

% first, check if the data is normalized
normal_distribution = adtest(tmp(:)); % 0, normal
% second , check if the data is equal variance
equal_variance = vartest2(tmp(:,1), tmp(:,2)); % 0, equal variance
% since dataset meets the normal distribution and equal variance, use
% paired t-test
[~, p, ci, stats] = ttest(tmp(:,1), tmp(:,2)); % t(8) = 5.0287, p = 0.0010

% >>> rest and run
tmp = [DFA_O2.pre.run, DFA_O2.post.run];

% nanmean(DFA_O2.pre.run)
% nanstd(DFA_O2.pre.run)
% 
% nanmean(DFA_O2.post.run)
% nanstd(DFA_O2.post.run)

% first, check if the data is normalized
normal_distribution = adtest(tmp(:)); % 0, normal
% second , check if the data is equal variance
equal_variance = vartest2(tmp(:,1), tmp(:,2)); % 0, equal variance
% since dataset meets the normal distribution and equal variance, use
% paired t-test
[~, p, ci, stats] = ttest(tmp(:,1), tmp(:,2)); % p = 0.0149, t(8) = 3.0876


%% Bar plot of DFA exponent: respiration, median +/- IQR
x1 = [1 2];
figure('name','DFA exponent before and after CNQX/AP5/muscimol infusion',...
    'position',[500 500 1200 400]);
h1 = subplot(161);
plot(x1,[DFA_resp.pre.rest(PC), DFA_resp.post.rest(PC)],...
    'marker','o','markersize',10,...
    'markeredgecolor','k','markerfacecolor',[0.5 0.5 0.5],'linewidth',1,...
    'color',[0.5 0.5 0.5]); % data points in FL/HL
hold on;
plot(x1,[DFA_resp.pre.rest(FC), DFA_resp.post.rest(FC)],...
    'marker','o','markersize',10,...
    'markeredgecolor',[0.91 0.41 0.17], 'markerfacecolor',[0.5 0.5 0.5],'linewidth',1,...
    'color',[0.5 0.5 0.5]); % data points in FC
boxplot([DFA_resp.pre.rest,DFA_resp.post.rest],x1,'Width',0.6,'Color','k','symbol','');
set(findobj(gcf,'-regexp','Tag','\w*Whisker'),'LineStyle','-');
hold on;
plot([x1(1)-0.3,x1(1)+0.3], nanmean(DFA_resp.pre.rest(:))*ones(1,2),'k--'); % plot mean as an extra
plot([x1(2)-0.3,x1(2)+0.3], nanmean(DFA_resp.post.rest(:))*ones(1,2),'k--');
axis([0.4 2.6 0 1.1]);
set(gca,'box','off','xtick',[],'xticklabel',[]);
ylabel('DFA exponent, respiration, rest');

h2 = subplot(162);
plot(x1,[DFA_resp.pre.run(PC), DFA_resp.post.run(PC)],...
    'marker','o','markersize',10,...
    'markeredgecolor','k','markerfacecolor',[0.5 0.5 0.5],'linewidth',1,...
    'color',[0.5 0.5 0.5]); % data points in FL/HL
hold on;
plot(x1,[DFA_resp.pre.run(FC), DFA_resp.post.run(FC)],...
    'marker','o','markersize',10,...
    'markeredgecolor',[0.91 0.41 0.17], 'markerfacecolor',[0.5 0.5 0.5],'linewidth',1,...
    'color',[0.5 0.5 0.5]); % data points in FC
boxplot([DFA_resp.pre.run,DFA_resp.post.run],x1,'Width',0.6,'Color','k','symbol','');
set(findobj(gcf,'-regexp','Tag','\w*Whisker'),'LineStyle','-');
hold on;
plot([x1(1)-0.3,x1(1)+0.3], nanmean(DFA_resp.pre.run(:))*ones(1,2),'k--'); % plot mean as an extra
plot([x1(2)-0.3,x1(2)+0.3], nanmean(DFA_resp.post.run(:))*ones(1,2),'k--');
axis([0.4 2.6 0 1.1]);
set(gca,'box','off','xtick',[],'xticklabel',[]);
ylabel('DFA exponent, respiration, all data');

savefig(gcf, fullfile(rootfolder, 'Figure', 'Fig_S6c'));
close(gcf);

%% Stats: DFA resp
% >>> rest
tmp = [DFA_resp.pre.rest, DFA_resp.post.rest];

% nanmean(DFA_resp.pre.rest) % 0.78 +/- 0.04
% nanstd(DFA_resp.pre.rest)
% 
% nanmean(DFA_resp.post.rest) % 0.77 +/- 0.05
% nanstd(DFA_resp.post.rest)

% first, check if the data is normalized
normal_distribution = adtest(tmp(:)); % 0, normal
% second , check if the data is equal variance
equal_variance = vartest2(tmp(:,1), tmp(:,2)); % 0, equal variance
[~, p, ci, stats] = ttest(tmp(:,1), tmp(:,2)); % p = 0.5288, t(8) = 0.6583

% >>> rest and run
tmp = [DFA_resp.pre.run, DFA_resp.post.run];

% nanmean(DFA_resp.pre.run) % 0.86 +/- 0.06
% nanstd(DFA_resp.pre.run)
% 
% nanmean(DFA_resp.post.run) % 0.81 +/- 0.06
% nanstd(DFA_resp.post.run)

% first, check if the data is normalized
normal_distribution = adtest(tmp(:)); % 0, normal
% second , check if the data is equal variance
equal_variance = vartest2(tmp(:,1), tmp(:,2)); % 0, equal variance
[~, p, ci, stats] = ttest(tmp(:,1), tmp(:,2)); % p = 0.0072, t(8) = 3.5835


%% Bar plot of DFA exponent: Gamma-band LFP, median +/- IQR
x1 = [1 2];
figure('name','DFA exponent before and after CNQX/AP5/muscimol infusion',...
    'position',[500 500 1200 400]);
h1 = subplot(161);
plot(x1,[DFA_Gamma.pre.rest(PC), DFA_Gamma.post.rest(PC)],...
    'marker','o','markersize',10,...
    'markeredgecolor','k','markerfacecolor',[0.5 0.5 0.5],'linewidth',1,...
    'color',[0.5 0.5 0.5]); % data points in FL/HL
hold on;
plot(x1,[DFA_Gamma.pre.rest(FC), DFA_Gamma.post.rest(FC)],...
    'marker','o','markersize',10,...
    'markeredgecolor',[0.91 0.41 0.17], 'markerfacecolor',[0.5 0.5 0.5],'linewidth',1,...
    'color',[0.5 0.5 0.5]); % data points in FC
boxplot([DFA_Gamma.pre.rest,DFA_Gamma.post.rest],x1,'Width',0.6,'Color','k','symbol','');
set(findobj(gcf,'-regexp','Tag','\w*Whisker'),'LineStyle','-');
hold on;
plot([x1(1)-0.3,x1(1)+0.3], nanmean(DFA_Gamma.pre.rest(:))*ones(1,2),'k--'); % plot mean as an extra
plot([x1(2)-0.3,x1(2)+0.3], nanmean(DFA_Gamma.post.rest(:))*ones(1,2),'k--');
axis([0.4 2.6 0 1]);
set(gca,'box','off','xtick',[],'xticklabel',[]);
ylabel('DFA exponent, Gamma BLP, rest');

h2 = subplot(162);
plot(x1,[DFA_Gamma.pre.run(PC), DFA_Gamma.post.run(PC)],...
    'marker','o','markersize',10,...
    'markeredgecolor','k','markerfacecolor',[0.5 0.5 0.5],'linewidth',1,...
    'color',[0.5 0.5 0.5]); % data points in FL/HL
hold on;
plot(x1,[DFA_Gamma.pre.run(FC), DFA_Gamma.post.run(FC)],...
    'marker','o','markersize',10,...
    'markeredgecolor',[0.91 0.41 0.17], 'markerfacecolor',[0.5 0.5 0.5],'linewidth',1,...
    'color',[0.5 0.5 0.5]); % data points in FC
boxplot([DFA_Gamma.pre.run,DFA_Gamma.post.run],x1,'Width',0.6,'Color','k','symbol','');
set(findobj(gcf,'-regexp','Tag','\w*Whisker'),'LineStyle','-');
hold on;
plot([x1(1)-0.3,x1(1)+0.3], nanmean(DFA_Gamma.pre.run(:))*ones(1,2),'k--'); % plot mean as an extra
plot([x1(2)-0.3,x1(2)+0.3], nanmean(DFA_Gamma.post.run(:))*ones(1,2),'k--');
axis([0.4 2.6 0 1]);
set(gca,'box','off','xtick',[],'xticklabel',[]);
ylabel('DFA exponent, Gamma BLP, all data');

savefig(gcf, fullfile(rootfolder, 'Figure', 'Fig_S5f'));
close(gcf);

%% Stats: DFA Gamma
% >>> rest
tmp = [DFA_Gamma.pre.rest, DFA_Gamma.post.rest];

% nanmean(DFA_Gamma.pre.rest) % 0.57 +/- 0.07
% nanstd(DFA_Gamma.pre.rest)
% 
% nanmean(DFA_Gamma.post.rest) % 0.51 +/- 0.09
% nanstd(DFA_Gamma.post.rest)

% first, check if the data is normalized
normal_distribution = adtest(tmp(:)); % 0, normal
% second , check if the data is equal variance
equal_variance = vartest2(tmp(:,1), tmp(:,2)); % 0, equal variance
% since dataset meets the normal distribution and equal variance, use
% paired t-test
[~, p, ci, stats] = ttest(tmp(:,1), tmp(:,2)); % t(8) = 1.5092, p = 0.1697

% >>> rest and run
tmp = [DFA_Gamma.pre.run, DFA_Gamma.post.run];

% nanmean(DFA_Gamma.pre.run) % 0.68 +/- 0.06
% nanstd(DFA_Gamma.pre.run)
% 
% nanmean(DFA_Gamma.post.run) % 0.56 +/- 0.17
% nanstd(DFA_Gamma.post.run)

% first, check if the data is normalized
normal_distribution = adtest(tmp(:)); % 1, not normal
% second , check if the data is equal variance
equal_variance = vartest2(tmp(:,1), tmp(:,2)); % 1, not equal variance
[p,h,stats] = ranksum(tmp(:,1), tmp(:,2)); % p = 0.1359

end