function figure_scatter_restSD_LFP_Oxy(out, NumMice, PC, FC)
%% Relationship between resting variance of oxygen and gamma-band LFP
OxyVar.pre = out.RestVar.oxy.pre;
OxyVar.pre = [OxyVar.pre{:}];
OxyVar.pre = reshape(OxyVar.pre,3,NumMice);

OxyVar.post = out.RestVar.oxy.post;
OxyVar.post = [OxyVar.post{:}];
OxyVar.post = reshape(OxyVar.post,3,NumMice);

Gamma.pre = out.RestVar.Gamma.pre;
Gamma.pre = [Gamma.pre{:}];
Gamma.pre = reshape(Gamma.pre,3,NumMice);

Gamma.post = out.RestVar.Gamma.post;
Gamma.post = [Gamma.post{:}];
Gamma.post = reshape(Gamma.post,3,NumMice);

gamma_var_ratio = Gamma.post(1,:)./Gamma.pre(1,:).*...
    Gamma.post(3,:)./Gamma.pre(3,:);
oxy_var_ratio = OxyVar.post(1,:)./OxyVar.pre(1,:);

compare_neural_CBV(gamma_var_ratio(:),oxy_var_ratio(:),'Gamma');
end