function plot_fig_4(rootfolder)
data = load(fullfile(rootfolder, 'Data','Oxygen_prediction','HRFPredict.mat'));

oxy_1st_power_raw = data.oxy_1st_power_raw;
oxy_1st_power_predict = data.oxy_1st_power_predict;
oxy_1st_slope_raw = data.oxy_1st_slope_raw;
oxy_1st_slope_predict = data.oxy_1st_slope_predict;
oxy_1st_std_raw = data.oxy_1st_std_raw;
oxy_1st_std_predict = data.oxy_1st_std_predict;
oxy_1st_HRF_smooth = data.oxy_1st_HRF_smooth;
oxy_1st_DFA_raw = data.oxy_1st_DFA_raw;
oxy_1st_DFA_predict = data.oxy_1st_DFA_predict;
freq = data.freq;

tt = 1500;
time = (0:tt-1)/30;
AA = oxy_1st_HRF_smooth(1:tt,:)./max(oxy_1st_HRF_smooth(1:tt,:), [],1);
BB = AA;
[zeroa, poleb, gain] = butter(5,2/30*1,'low');
[sos,g] = zp2sos(zeroa,poleb, gain);
for kk =1:size(AA,2)
    BB(:,kk) = filtfilt(sos,g,AA(:,kk));
end

CC = BB(:,2:9);

figure('name','HRF: oxygen, gamma-band LFP');
box_line(time, mean(CC,2), std(CC,[],2)/sqrt(8),'k'); hold on;
plot(time, mean(CC,2),'k');
xlabel('Time (s)'); ylabel('HRF');
savefig(gcf, fullfile(rootfolder, 'Figure', 'Fig_4b'));
close(gcf);


raw_ave = mean(oxy_1st_power_raw,2);
predict_ave = mean(oxy_1st_power_predict,2);
raw_sem = std(oxy_1st_power_raw,[],2)/3;
predict_sem = std(oxy_1st_power_predict,[],2)/3;


figure; 
pt1 = box_line(freq(2:end),raw_ave(2:end),raw_sem(2:end), 'k'); hold on;
plot(freq(2:end), raw_ave(2:end),'k');
plot(freq(2:end), 10.^(mean(oxy_1st_slope_raw)*log10(freq(2:end))-2),'k--')
set(gca, 'xscale','log', 'yscale','log');
axis square;
pt2 = box_line(freq(2:end),predict_ave(2:end),predict_sem(2:end), 'r'); hold on;
plot(freq(2:end), predict_ave(2:end),'r');
plot(freq(2:end), 10.^(mean(oxy_1st_slope_predict)*log10(freq(2:end))),'r--')
set(gca, 'xscale','log', 'yscale','log');
axis([0.01 1 1e-3 1e3]);
axis square;
ylabel('Oxygen power (mmHg^2/Hz)');
xlabel('Frequency (Hz)');
savefig(gcf, fullfile(rootfolder, 'Figure', 'Fig_4c'));
close(gcf);
%% power spectrum exponent
FC = [2,3,5,8,9];
PC = [1,4,6,7];


% Median +/- IQR
x1 = [1 2];
figure('position',[100 100 800 400]);
h1 = subplot(141);
plot(x1,-[oxy_1st_slope_raw(PC);oxy_1st_slope_predict(PC)]',...
    'marker','o','markersize',10,...
    'markeredgecolor','k','markerfacecolor',[0.5 0.5 0.5],'linewidth',1,...
    'color',[0.5 0.5 0.5]); % data points in FL/HL
hold on;
plot(x1,-[oxy_1st_slope_raw(FC);oxy_1st_slope_predict(FC)]',...
    'marker','o','markersize',10,...
    'markeredgecolor','k','markerfacecolor',[0.91 0.41 0.17],'linewidth',1,...
    'color',[0.5 0.5 0.5]); % data points in FL/HL
boxplot(-[oxy_1st_slope_raw',oxy_1st_slope_predict'],x1,'Width',0.6,'Color','k','symbol','');
set(findobj(gcf,'-regexp','Tag','\w*Whisker'),'LineStyle','-');
hold on;
plot([x1(1)-0.3,x1(1)+0.3], nanmean(-oxy_1st_slope_raw(:))*ones(1,2),'k--'); % plot mean as an extra
plot([x1(2)-0.3,x1(2)+0.3], nanmean(-oxy_1st_slope_predict(:))*ones(1,2),'k--');
axis([0.4 2.6 0 2.5]);
set(gca,'box','off','xticklabel',[]);
ylabel('Power-law exponent');
savefig(gcf, fullfile(rootfolder, 'Figure', 'Fig_4d'));
close(gcf);


% mean(oxy_1st_slope_raw)
% std(oxy_1st_slope_raw) % 1.81 +/- 0.38
% 
% mean(oxy_1st_slope_predict)
% std(oxy_1st_slope_predict) % 1.09 +/- 0.44

% first, check if the data is normalized
normal_distribution = adtest([oxy_1st_slope_raw(:);oxy_1st_slope_predict(:);]); % 0, normal
% second , check if the data is equal variance
equal_variance = vartest2(oxy_1st_slope_raw(:),oxy_1st_slope_predict(:)); % 0, equal variance
[h,p,ci,stats] = ttest(oxy_1st_slope_raw(:),oxy_1st_slope_predict(:)); % P = 0.0093, t(8) = 3.4059
disp('');

%% DFA
FC = [2,3,5,8,9];
PC = [1,4,6,7];

% Median +/- IQR
x1 = [1 2];
figure('position',[100 100 800 400]);
h1 = subplot(141);
plot(x1,[oxy_1st_DFA_raw(PC);oxy_1st_DFA_predict(PC)]',...
    'marker','o','markersize',10,...
    'markeredgecolor','k','markerfacecolor',[0.5 0.5 0.5],'linewidth',1,...
    'color',[0.5 0.5 0.5]); % data points in FL/HL
hold on;
plot(x1,[oxy_1st_DFA_raw(FC);oxy_1st_DFA_predict(FC)]',...
    'marker','o','markersize',10,...
    'markeredgecolor','k','markerfacecolor',[0.91 0.41 0.17],'linewidth',1,...
    'color',[0.5 0.5 0.5]); % data points in FL/HL
boxplot([oxy_1st_DFA_raw',oxy_1st_DFA_predict'],x1,'Width',0.6,'Color','k','symbol','');
set(findobj(gcf,'-regexp','Tag','\w*Whisker'),'LineStyle','-');
hold on;
plot([x1(1)-0.3,x1(1)+0.3], nanmean(oxy_1st_DFA_raw(:))*ones(1,2),'k--'); % plot mean as an extra
plot([x1(2)-0.3,x1(2)+0.3], nanmean(oxy_1st_DFA_predict(:))*ones(1,2),'k--');
axis([0.4 2.6 0 2]);
set(gca,'box','off','xticklabel',[]);
ylabel('DFA scaling exponent');
savefig(gcf, fullfile(rootfolder, 'Figure', 'Fig_4e'));
close(gcf);


% mean(oxy_1st_DFA_raw)
% std(oxy_1st_DFA_raw) % 1.41 +/- 0.18
% 
% mean(oxy_1st_DFA_predict)
% std(oxy_1st_DFA_predict) % 1.11 +/- 0.20

% first, check if the data is normalized
normal_distribution = adtest([oxy_1st_DFA_raw(:);oxy_1st_DFA_predict(:);]); % 0, normal
% second , check if the data is equal variance
equal_variance = vartest2(oxy_1st_DFA_raw(:),oxy_1st_DFA_predict(:)); % 0, equal variance
[h,p,ci,stats] = ttest(oxy_1st_DFA_raw(:),oxy_1st_DFA_predict(:)); % P = 0.0229, t(8) = 2.8085

%% power change below 0.1 Hz
seg_freq = (freq>0)&(freq<=0.1);

power_raw = oxy_1st_power_raw(seg_freq,:);
power_raw_ave = mean(power_raw);
power_predict = oxy_1st_power_predict(seg_freq,:);
power_predict_ave = mean(power_predict);
% mean((power_predict_ave-power_raw_ave)./power_raw_ave)

% first, check if the data is normalized
normal_distribution = adtest([power_raw_ave(:);power_predict_ave(:);]); % 0, normal
% second , check if the data is equal variance
equal_variance = vartest2(power_raw_ave(:),power_predict_ave(:)); % 0, equal variance
[h,p,ci,stats] = ttest(power_raw_ave(:),power_predict_ave(:)); % P = 0.0048, t(8) = 3.8597

end