function plot_coh_BLP_Resp_O2(freq, coh_Gamma, coh_Beta, coh_SubAlpha, coh_Resp, ...
    coh_ci_Gamma,coh_ci_Beta,coh_ci_SubAlpha,coh_ci_Resp, rootfolder)
figure('name','coherence btwn. BLP and O2','position',[300 300 600 1070]);
h1 = subplot(421);
pt1 = box_line(freq, nanmean(coh_Gamma.rest.pre,1),...
    nanstd(coh_Gamma.rest.pre,[],1)/sqrt(length(find(~isnan(coh_Gamma.rest.pre(:,1))))),...
    'k'); hold on;
plot(freq, nanmean(coh_Gamma.rest.pre,1),'k');
plot(freq, nanmean(coh_ci_Gamma.rest.pre.ub,1),'k');
% pt2 = box_line(freq, nanmean(coh_Gamma.rest.post,1),...
%     nanstd(coh_Gamma.rest.post,[],1)/sqrt(length(find(~isnan(coh_Gamma.rest.post(:,1))))),...
%     'r'); hold on;
% plot(freq, nanmean(coh_Gamma.rest.post,1),'r');
% plot(freq, nanmean(coh_ci_Gamma.rest.post.ub,1),'r');
set(gca, 'xscale','log');
axis([0 1 0 1]);
title(['Rest (n = ',num2str(length(find(~isnan(coh_Gamma.rest.pre(:,1))))),')']);
% legend([pt1, pt2],{'aCSF'; 'CNQX/AP5/muscimol'});
xlabel('Frequency (Hz)');
ylabel('Coherence');

% h2 = subplot(422);
% pt1 = box_line(freq, nanmean(coh_Gamma.run.pre,1),...
%     nanstd(coh_Gamma.run.pre,[],1)/sqrt(length(find(~isnan(coh_Gamma.run.pre(:,1))))),...
%     'k'); hold on;
% plot(freq, nanmean(coh_Gamma.run.pre,1),'k');
% plot(freq, nanmean(coh_ci_Gamma.run.pre.ub,1),'k');
% pt2 = box_line(freq, nanmean(coh_Gamma.run.post,1),...
%     nanstd(coh_Gamma.run.post,[],1)/sqrt(length(find(~isnan(coh_Gamma.run.post(:,1))))),...
%     'r'); hold on;
% plot(freq, nanmean(coh_Gamma.run.post,1),'r');
% plot(freq, nanmean(coh_ci_Gamma.run.post.ub,1),'r');
% set(gca, 'xscale','log');
% axis([0 1 0 1]);
% title(['Run (n = ',num2str(length(find(~isnan(coh_Gamma.run.pre(:,1))))),')']);
% legend([pt1, pt2],{'aCSF'; 'CNQX/AP5/muscimol'});
% xlabel('Frequency (Hz)');
% ylabel('Coherence');

h3 = subplot(423);
pt1 = box_line(freq, nanmean(coh_Beta.rest.pre,1),...
    nanstd(coh_Beta.rest.pre,[],1)/sqrt(length(find(~isnan(coh_Beta.rest.pre(:,1))))),...
    'k'); hold on;
plot(freq, nanmean(coh_Beta.rest.pre,1),'k');
plot(freq, nanmean(coh_ci_Beta.rest.pre.ub,1),'k');
% pt2 = box_line(freq, nanmean(coh_Beta.rest.post,1),...
%     nanstd(coh_Beta.rest.post,[],1)/sqrt(length(find(~isnan(coh_Beta.rest.post(:,1))))),...
%     'r'); hold on;
% plot(freq, nanmean(coh_Beta.rest.post,1),'r');
% plot(freq, nanmean(coh_ci_Beta.rest.post.ub,1),'r');
set(gca, 'xscale','log');
axis([0 1 0 1]);
title(['Rest (n = ',num2str(length(find(~isnan(coh_Beta.rest.pre(:,1))))),')']);
% legend([pt1, pt2],{'aCSF'; 'CNQX/AP5/muscimol'});
xlabel('Frequency (Hz)');
ylabel('Coherence');

% h4 = subplot(424);
% pt1 = box_line(freq, nanmean(coh_Beta.run.pre,1),...
%     nanstd(coh_Beta.run.pre,[],1)/sqrt(length(find(~isnan(coh_Beta.run.pre(:,1))))),...
%     'k'); hold on;
% plot(freq, nanmean(coh_Beta.run.pre,1),'k');
% plot(freq, nanmean(coh_ci_Beta.run.pre.ub,1),'k');
% pt2 = box_line(freq, nanmean(coh_Beta.run.post,1),...
%     nanstd(coh_Beta.run.post,[],1)/sqrt(length(find(~isnan(coh_Beta.run.post(:,1))))),...
%     'r'); hold on;
% plot(freq, nanmean(coh_Beta.run.post,1),'r');
% plot(freq, nanmean(coh_ci_Beta.run.post.ub,1),'r');
% set(gca, 'xscale','log');
% axis([0 1 0 1]);
% title(['Run (n = ',num2str(length(find(~isnan(coh_Beta.run.pre(:,1))))),')']);
% legend([pt1, pt2],{'aCSF'; 'CNQX/AP5/muscimol'});
% xlabel('Frequency (Hz)');
% ylabel('Coherence');

h5 = subplot(425);
pt1 = box_line(freq, nanmean(coh_SubAlpha.rest.pre,1),...
    nanstd(coh_SubAlpha.rest.pre,[],1)/sqrt(length(find(~isnan(coh_SubAlpha.rest.pre(:,1))))),...
    'k'); hold on;
plot(freq, nanmean(coh_SubAlpha.rest.pre,1),'k');
plot(freq, nanmean(coh_ci_SubAlpha.rest.pre.ub,1),'k');
% pt2 = box_line(freq, nanmean(coh_SubAlpha.rest.post,1),...
%     nanstd(coh_SubAlpha.rest.post,[],1)/sqrt(length(find(~isnan(coh_SubAlpha.rest.post(:,1))))),...
%     'r'); hold on;
% plot(freq, nanmean(coh_SubAlpha.rest.post,1),'r');
% plot(freq, nanmean(coh_ci_SubAlpha.rest.post.ub,1),'r');
set(gca, 'xscale','log');
axis([0 1 0 1]);
title(['Rest (n = ',num2str(length(find(~isnan(coh_SubAlpha.rest.pre(:,1))))),')']);
% legend([pt1, pt2],{'aCSF'; 'CNQX/AP5/muscimol'});
xlabel('Frequency (Hz)');
ylabel('Coherence');

% h6 = subplot(426);
% pt1 = box_line(freq, nanmean(coh_SubAlpha.run.pre,1),...
%     nanstd(coh_SubAlpha.run.pre,[],1)/sqrt(length(find(~isnan(coh_SubAlpha.run.pre(:,1))))),...
%     'k'); hold on;
% plot(freq, nanmean(coh_SubAlpha.run.pre,1),'k');
% plot(freq, nanmean(coh_ci_SubAlpha.run.pre.ub,1),'k');
% pt2 = box_line(freq, nanmean(coh_SubAlpha.run.post,1),...
%     nanstd(coh_SubAlpha.run.post,[],1)/sqrt(length(find(~isnan(coh_SubAlpha.run.post(:,1))))),...
%     'r'); hold on;
% plot(freq, nanmean(coh_SubAlpha.run.post,1),'r');
% plot(freq, nanmean(coh_ci_SubAlpha.run.post.ub,1),'k');
% set(gca, 'xscale','log');
% axis([0 1 0 1]);
% title(['Run (n = ',num2str(length(find(~isnan(coh_SubAlpha.run.pre(:,1))))),')']);
% legend([pt1, pt2],{'aCSF'; 'CNQX/AP5/muscimol'});
% xlabel('Frequency (Hz)');
% ylabel('Coherence');

h7 = subplot(427);
pt1 = box_line(freq, nanmean(coh_Resp.rest.pre,1),...
    nanstd(coh_Resp.rest.pre,[],1)/sqrt(length(find(~isnan(coh_Resp.rest.pre(:,1))))),...
    'k'); hold on;
plot(freq, nanmean(coh_Resp.rest.pre,1),'k');
plot(freq, nanmean(coh_ci_Resp.rest.pre.ub,1),'k');
% pt2 = box_line(freq, nanmean(coh_Resp.rest.post,1),...
%     nanstd(coh_Resp.rest.post,[],1)/sqrt(length(find(~isnan(coh_Resp.rest.post(:,1))))),...
%     'r'); hold on;
% plot(freq, nanmean(coh_Resp.rest.post,1),'r');
% plot(freq, nanmean(coh_ci_Resp.rest.post.ub,1),'r');
set(gca, 'xscale','log');
axis([0 1 0 1]);
title(['Rest (n = ',num2str(length(find(~isnan(coh_Resp.rest.pre(:,1))))),')']);
% legend([pt1, pt2],{'aCSF'; 'CNQX/AP5/muscimol'});
xlabel('Frequency (Hz)');
ylabel('Coherence');

% h8 = subplot(428);
% pt1 = box_line(freq, nanmean(coh_Resp.run.pre,1),...
%     nanstd(coh_Resp.run.pre,[],1)/sqrt(length(find(~isnan(coh_Resp.run.pre(:,1))))),...
%     'k'); hold on;
% plot(freq, nanmean(coh_Resp.run.pre,1),'k');
% plot(freq, nanmean(coh_ci_Resp.run.pre.ub,1),'k');
% pt2 = box_line(freq, nanmean(coh_Resp.run.post,1),...
%     nanstd(coh_Resp.run.post,[],1)/sqrt(length(find(~isnan(coh_Resp.run.post(:,1))))),...
%     'r'); hold on;
% plot(freq, nanmean(coh_Resp.run.post,1),'r');
% plot(freq, nanmean(coh_ci_Resp.run.post.ub,1),'r');
% set(gca, 'xscale','log');
% axis([0 1 0 1]);
% title(['Run (n = ',num2str(length(find(~isnan(coh_Resp.run.pre(:,1))))),')']);
% legend([pt1, pt2],{'aCSF'; 'CNQX/AP5/muscimol'});
% xlabel('Frequency (Hz)');
% ylabel('Coherence');

end