function plot_fig_1bcd_2c(rootfolder)
%% Example trace showing resting oxygen fluctuations and power-law fitting
% Mouse ID: QZ0360, Depth: DV = 300 um, 543-603 seconds, 105-165 s
search_folder = fullfile(rootfolder, 'Data', 'QZ0360_DV300');
filen_QZ360 = getfilenames(search_folder, '*.mat');
if numel(filen_QZ360)==1
    data_QZ360 = load(filen_QZ360{1});
    plaw_QZ360 = load(filen_QZ360{1});
else
    error('More than one file exist');
end
clear search_folder filen_QZ360;

%% Example trace showing resting oxygen power spectrum and its power-law fit in a dead mouse
search_folder = fullfile(rootfolder, 'Data', 'QZ0373_DV300');
filen_QZ373 = getfilenames(search_folder, '*.mat');
plaw_QZ373 = load(filen_QZ373{1});
clear search_folder filen_QZ373;

%% data_QZ360: get data chunks for plot
% get resting data segment
oxy_Fs = 30; % oxygen trace was downsampled to 30 Hz
neural_Fs = 30000; % neural activity was sampled at 30 kHz
seg_tmp = [553, 603]; % seg_tmp = [105, 165];

seg = seg_tmp(1)*oxy_Fs:seg_tmp(2)*oxy_Fs;
oxy_QZ360 = data_QZ360.oxygen(seg);
neural_QZ360 = data_QZ360.LFP(seg_tmp(1)*neural_Fs:seg_tmp(2)*neural_Fs);
Gamma_power_QZ360 = data_QZ360.LFP_envelope.raw.Gamma.ds30_filt(seg);
Beta_power_QZ360 = data_QZ360.LFP_envelope.raw.Beta.ds30_filt(seg);
SubAlpha_power_QZ360 = data_QZ360.LFP_envelope.raw.SubAlpha.ds30_filt(seg);
time_ds30 = seg_tmp(1):1/oxy_Fs:seg_tmp(2);

% generate figure
figure('name','Resting oxygen tension (300 microns deep): an example, resting segment');
% axes for resting oxygen tension
h1 = subplot(211);
plot(time_ds30,oxy_QZ360,'color','k');
ylabel('PtO_2 (mmHg)'); xlabel('Time (s)');
xlim([seg_tmp(1) seg_tmp(2)]);
set(h1,'xtick', seg_tmp(1):10:seg_tmp(2),'xticklabel',(seg_tmp(1):10:seg_tmp(2))-seg_tmp(1));
h2 = subplot(212);
hold on;
plot(time_ds30,Gamma_power_QZ360*20,'color','k');
plot(time_ds30,Beta_power_QZ360*5,'color','r');
plot(time_ds30,SubAlpha_power_QZ360,'color','c');
legend({'Gamma*20','Beta*5','SubAlpha'});
ylabel('Band-limited power change'); xlabel('Time (s)');
xlim([seg_tmp(1) seg_tmp(2)]);
set(h2,'xtick', seg_tmp(1):10:seg_tmp(2),'xticklabel',(seg_tmp(1):10:seg_tmp(2))-seg_tmp(1));
linkaxes([h1,h2],'x');

savefig(gcf, fullfile(rootfolder, 'Figure', 'Fig_1b'));
close(gcf);

%% Process for converting raw ephys data to band-limited signal
% 1. get LFP
LFP_QZ360 = data_QZ360.LFP;
% high pass filter of neural data
[zeroa, poleb, gain] = butter(5,2/neural_Fs*1, 'high');
[sos,g] = zp2sos(zeroa,poleb, gain);
tmp = filtfilt(sos,g, LFP_QZ360);

% band pass filter of neural data, working on the high-passed oxygen data
[zeroa, poleb, gain] = butter(5,2/neural_Fs*300, 'low');
[sos,g] = zp2sos(zeroa,poleb, gain);
LFP_QZ360 = filtfilt(sos,g, tmp);

% 2. get Gamma-band power
Gamma_QZ360 = LFP_envelope_2(LFP_QZ360, neural_Fs, 'Gamma');
Beta_QZ360 = LFP_envelope_2(LFP_QZ360, neural_Fs, 'Beta');
SubAlpha_QZ360 = LFP_envelope_2(LFP_QZ360, neural_Fs, 'SubAlpha');

time = seg_tmp(1):1/neural_Fs:seg_tmp(2);

% generate figure
figure('name','Example showing process for converting raw ephys data to band-limited signal');
h(1) = subplot(411);
plot(time, LFP_QZ360(seg_tmp(1)*neural_Fs:seg_tmp(2)*neural_Fs));
ylabel('LFP');
h(2) = subplot(412);
plot(time, Gamma_QZ360.data_filt(seg_tmp(1)*neural_Fs:seg_tmp(2)*neural_Fs));
ylabel('Gamma');
h(3) = subplot(413);
plot(time, Gamma_QZ360.data_rect(seg_tmp(1)*neural_Fs:seg_tmp(2)*neural_Fs));
ylabel('Gamma rectify');
h(4) = subplot(414);
plot(time, Gamma_QZ360.data_rect_filt(seg_tmp(1)*neural_Fs:seg_tmp(2)*neural_Fs));
ylabel('Gamma rectify filt');
set(h,'xtick', seg_tmp(1):1:seg_tmp(2),'xticklabel',(seg_tmp(1):1:seg_tmp(2))-seg_tmp(1));
linkaxes(h,'x');
xlabel('Time (s)');
xlim([588 596]);

savefig(gcf, fullfile(rootfolder, 'Figure', 'Fig_2c'));
close(gcf);

%% Plot oxygen spectral power and its power-law fit
monitorPos = get(0,'MonitorPositions'); % [x y w ht] X # of monitors
w = 1200; ht = 600; % Define window size: width(w) and height(ht)
mon = 1; % Only one monitor

% Determine positioning for main figure
pos = [((monitorPos(mon,3)-w)/2)+monitorPos(mon,1)/2,...
    ((monitorPos(mon,4)-ht)/2)+monitorPos(mon,2)/2,...
    w ht];

figure('position',pos,'color','w',...
    'name','Oxygen fluctuation and band-limited LFP power spectrum (300 microns deep): an example');

h11 = axes('units','normalized','position',[0.05 0.10 0.4 0.8]);
hold on;
plot(plaw_QZ360.frest_oxy,plaw_QZ360.prest_oxy,'color','g');
plot(plaw_QZ360.frun_oxy,plaw_QZ360.prun_oxy,'color','m')
plot(plaw_QZ373.frun_oxy,plaw_QZ373.prun_oxy,'color','c'); % NOTE: since QZ0373 is a dead mouse, rest and run are the same
plot(plaw_QZ360.Oxy_plaw.rest.LSERout.freq_resamp, 10.^(plaw_QZ360.Oxy_plaw.rest.LSERout.predict), 'color','g');
plot(plaw_QZ360.Oxy_plaw.run.LSERout.freq_resamp, 10.^(plaw_QZ360.Oxy_plaw.run.LSERout.predict), 'color','m');
plot(plaw_QZ373.Oxy_plaw.run.LSERout.freq_resamp, 10.^(plaw_QZ373.Oxy_plaw.run.LSERout.predict), 'color','c');
set(gca,'box','off',...
    'xscale','log','yscale','log',...
    'xlim',[0.01 1],...
    'xticklabel',{'0.01','0.1', '1'});
text(0.1,0.2,...
    ['Rest: log10(Power) = ', num2str(plaw_QZ360.Oxy_plaw.rest.LSERout.slope), '* log10(f) + (', num2str(plaw_QZ360.Oxy_plaw.rest.LSERout.intercept),')'],...
    'unit','normalized','color','g');
text(0.1,0.25,...
    ['All data: log10(Power) = ', num2str(plaw_QZ360.Oxy_plaw.run.LSERout.slope), '* log10(f) + (', num2str(plaw_QZ360.Oxy_plaw.run.LSERout.intercept),')'],...
    'unit','normalized','color','m');
text(0.1,0.15,...
    ['Dead mouse: log10(Power) = ', num2str(plaw_QZ373.Oxy_plaw.run.LSERout.slope), '* log10(f) + (', num2str(plaw_QZ373.Oxy_plaw.run.LSERout.intercept),')'],...
    'unit','normalized','color','c');
ylabel('Power (mmHg^2/Hz)');
legend('Rest','All data','Dead mouse');
title('Oxygen power spectrum: rest');

h12 = axes('units','normalized',...
    'position',[0.05 0.10-0.16/2 0.4 0.16],...
    'box','off'); % plot residual
hold on;
ylim([-1.4 1.4]);
plot(plaw_QZ360.Oxy_plaw.rest.LSERout.freq_resamp,plaw_QZ360.Oxy_plaw.rest.LSERout.residual,'color','g');
plot(plaw_QZ360.Oxy_plaw.run.LSERout.freq_resamp,plaw_QZ360.Oxy_plaw.run.LSERout.residual,'color','m');
plot(plaw_QZ373.Oxy_plaw.run.LSERout.freq_resamp,plaw_QZ373.Oxy_plaw.run.LSERout.residual,'color','c');
set(gca,'xscale','log',...
    'yaxislocation','right',...
    'xtick',[],...
    'color','none',...
    'xcolor','w');
xlabel('Frequency (Hz)','color','k');
ylabel('log10(Power)'); % wait for power spectrum during rest and running


h21 = axes('units','normalized','position',[0.55 0.10 0.4 0.8]);
loglog(plaw_QZ373.DFA_run.oxy.timeScale/30,plaw_QZ373.DFA_run.oxy.Fn,'co',...
    plaw_QZ373.DFA_run.oxy.timeScale/30,exp(plaw_QZ373.DFA_run.oxy.logfit),'k');
hold on;
loglog(plaw_QZ360.DFA_rest.oxy.timeScale/30,plaw_QZ360.DFA_rest.oxy.Fn,'go',...
    plaw_QZ360.DFA_rest.oxy.timeScale/30,exp(plaw_QZ360.DFA_rest.oxy.logfit),'k');
loglog(plaw_QZ360.DFA_run.oxy.timeScale/30,plaw_QZ360.DFA_run.oxy.Fn,'mo',...
    plaw_QZ360.DFA_run.oxy.timeScale/30,exp(plaw_QZ360.DFA_run.oxy.logfit),'k');
text(0.1,0.2,...
    ['logfit = ', num2str(plaw_QZ360.DFA_run.oxy.coefficient(1)),...
    '* logtimeScale + (', num2str(plaw_QZ360.DFA_run.oxy.coefficient(2)),')'],...
    'unit','normalized','color','m');
text(0.1,0.25,...
    ['logfit = ', num2str(plaw_QZ360.DFA_rest.oxy.coefficient(1)),...
    '* logtimeScale + (', num2str(plaw_QZ360.DFA_rest.oxy.coefficient(2)),')'],...
    'unit','normalized','color','g');
text(0.1,0.15,...
    ['logfit = ', num2str(plaw_QZ373.DFA_run.oxy.coefficient(1)),...
    '* logtimeScale + (', num2str(plaw_QZ373.DFA_run.oxy.coefficient(2)),')'],...
    'unit','normalized','color','c');
axis([1 60 0.1 400])
xlabel('Time scale n (second)'); 
ylabel('Detrended fluctuation function');

savefig(gcf, fullfile(rootfolder, 'Figure', 'Fig_1cd'));
close(gcf);
end


function out = LFP_envelope_2(data, Fs, fband)
% INPUT:
%       data: LFP signal
%       fband: different frequency band


% define different frequency band, though we only interested in gamma
% band
switch fband
    case 'Gamma'
        fpass = [40, 100];
    case 'LowGamma'
        fpass = [40, 55]; % set high limit to 55 to avoid powerline noise
    case 'HiGamma'
        fpass = [65, 100]; % set low limit to 65 to avoid powerline noise
    case 'Beta'
        fpass = [10, 30];
    case 'SubAlpha'
        fpass = [0.1, 8];
    case 'MUA'
        fpass = [300 3000];
end

% == get the band-limited signal
% Band pass neural data, first high pass, then low pass, this can reduce
% the boundary effects
% high pass filter of neural data
[zeroa, poleb, gain] = butter(5,2/Fs*fpass(1), 'high');
[sos,g] = zp2sos(zeroa,poleb, gain);
tmp = filtfilt(sos,g, data);

% band pass filter of neural data, working on the high-passed oxygen data
[zeroa, poleb, gain] = butter(5,2/Fs*fpass(2), 'low');
[sos,g] = zp2sos(zeroa,poleb, gain);
data_filt = filtfilt(sos,g, tmp);
out.data_filt = data_filt;

% == get the band-limited power of the signal we got above
data_rect = data_filt.^2; % rectify signal to get the power
out.data_rect = data_rect;

% == fitler the band-limited power below 1 Hz to match with oxygen signal
[zeroa, poleb, gain] = butter(5,2/Fs*1, 'low');
[sos,g] = zp2sos(zeroa,poleb, gain);
data_rect_filt = filtfilt(sos,g, data_rect);
out.data_rect_filt = data_rect_filt;

end