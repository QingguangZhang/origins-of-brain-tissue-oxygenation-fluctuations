function plot_fig_5c(rootfolder)
data = load(fullfile(rootfolder, 'Data','RestVar_drug_scatter.mat'));

NumMice = numel(data.animal); % how many animals
FC = [2,3,5,8,9]; % measurements in frontal cortex
PC = [1,4,6,7]; % measurements in FL/HL

figure_scatter_restSD_LFP_Oxy(data, NumMice, PC, FC);

savefig(gcf, fullfile(rootfolder, 'Figure', 'Fig_5c'));
close(gcf);
end