function plot_laminar_slope_LFP(freq, power, slope, num_freq, DFA, ephys_type, PC_num, FC_num, fig_filen)
%% oxygen power spectrum
% Normalize power spectrum to avoid bias
power_norm.(ephys_type) = normalized_power(power.(ephys_type), num_freq.(ephys_type));

% remove MA010 DV800 data due to noise
if ismember(ephys_type,{'Gamma','Beta','SubAlpha'})
    power_norm.(ephys_type).PC.DV800(1,:) = NaN;
    slope.(ephys_type).rest.PC(1,4) = NaN;
    slope.(ephys_type).run.PC(1,4) = NaN;
end

% Group average of oxygen power spectrum for plot
[power_norm_ave.(ephys_type), power_norm_sem.(ephys_type)] = ...
    group_average_power_O2(power_norm.(ephys_type));

% determine colormap for the following figures
colorlist = cbrewer('qual', 'Set2', 4);

axis_plot.LFP = [1 100 10^-4 10^2];
axis_plot.Gamma = [0.009 1 10^-1 10^1];
axis_plot.Beta = [0.009 1 10^-1.2 10^1];
axis_plot.SubAlpha = [0.009 1 10^-1 10^1];
axis_plot.MUA = [1 100 10^-1 10^1.2];

%% FC and PC together: power spectrum
figure('name',[ephys_type, ' power spectrum at different cortical depths: FC+PC']);
h1 = subplot(121);
pt1 = box_line(freq.(ephys_type), power_norm_ave.(ephys_type).rest.brain.DV100,...
    power_norm_sem.(ephys_type).rest.brain.DV100,  colorlist(1,:)); hold on;
plot(freq.(ephys_type), power_norm_ave.(ephys_type).rest.brain.DV100, 'color', colorlist(1,:));
plot(freq.(ephys_type), 10.^(mean([slope.(ephys_type).rest.PC(:,1);slope.(ephys_type).rest.FC(:,1);])*log10(freq.(ephys_type))),'color', colorlist(1,:));
set(gca, 'xscale','log', 'yscale','log');
axis square;
pt2 = box_line(freq.(ephys_type), power_norm_ave.(ephys_type).rest.brain.DV300,...
    power_norm_sem.(ephys_type).rest.brain.DV300, colorlist(2,:)); hold on;
plot(freq.(ephys_type), power_norm_ave.(ephys_type).rest.brain.DV300, 'color', colorlist(2,:));
plot(freq.(ephys_type), 10.^(mean([slope.(ephys_type).rest.PC(:,2);slope.(ephys_type).rest.FC(:,2);])*log10(freq.(ephys_type))),'color', colorlist(2,:));
set(gca, 'xscale','log', 'yscale','log');
axis square;
pt3 = box_line(freq.(ephys_type), power_norm_ave.(ephys_type).rest.brain.DV500,...
    power_norm_sem.(ephys_type).rest.brain.DV500, colorlist(3,:)); hold on;
plot(freq.(ephys_type), power_norm_ave.(ephys_type).rest.brain.DV500, 'color', colorlist(3,:));
plot(freq.(ephys_type), 10.^(mean([slope.(ephys_type).rest.PC(:,3);slope.(ephys_type).rest.FC(:,3);])*log10(freq.(ephys_type))),'color', colorlist(3,:));
set(gca, 'xscale','log', 'yscale','log');
axis square;
pt4 = box_line(freq.(ephys_type), power_norm_ave.(ephys_type).rest.brain.DV800,...
    power_norm_sem.(ephys_type).rest.brain.DV800, colorlist(4,:)); hold on;
plot(freq.(ephys_type), power_norm_ave.(ephys_type).rest.brain.DV800, 'color', colorlist(4,:));
plot(freq.(ephys_type), 10.^(nanmean([slope.(ephys_type).rest.PC(:,4);slope.(ephys_type).rest.FC(:,4);])*log10(freq.(ephys_type))),'color', colorlist(4,:));
axis(axis_plot.(ephys_type));
set(gca, 'xscale','log', 'yscale','log');
axis square;
title(['rest (n = ',num2str(size(power_norm.(ephys_type).rest.brain.DV100,1)),')']);
legend([pt1, pt2, pt3, pt4],...
    {'100 \mum'; '300 \mum'; '500 \mum'; '800 \mum'});
xlabel('Frequency (Hz)');
ylabel('Power (mmHg^2/Hz)');

h2 = subplot(122);
pt1 = box_line(freq.(ephys_type), power_norm_ave.(ephys_type).run.brain.DV100,...
    power_norm_sem.(ephys_type).run.brain.DV100, colorlist(1,:)); hold on;
plot(freq.(ephys_type), power_norm_ave.(ephys_type).run.brain.DV100, 'color', colorlist(1,:));
plot(freq.(ephys_type), 10.^(mean([slope.(ephys_type).run.PC(:,1);slope.(ephys_type).run.FC(:,1);])*log10(freq.(ephys_type))),'color', colorlist(1,:));
set(gca, 'xscale','log', 'yscale','log');
axis square;
pt2 = box_line(freq.(ephys_type), power_norm_ave.(ephys_type).run.brain.DV300,...
    power_norm_sem.(ephys_type).run.brain.DV300, colorlist(2,:)); hold on;
plot(freq.(ephys_type), power_norm_ave.(ephys_type).run.brain.DV300, 'color', colorlist(2,:));
plot(freq.(ephys_type), 10.^(mean([slope.(ephys_type).run.PC(:,2);slope.(ephys_type).run.FC(:,2);])*log10(freq.(ephys_type))),'color', colorlist(2,:));
set(gca, 'xscale','log', 'yscale','log');
axis square;
pt3 = box_line(freq.(ephys_type), power_norm_ave.(ephys_type).run.brain.DV500,...
    power_norm_sem.(ephys_type).run.brain.DV500, colorlist(3,:)); hold on;
plot(freq.(ephys_type), power_norm_ave.(ephys_type).run.brain.DV500, 'color', colorlist(3,:));
plot(freq.(ephys_type), 10.^(mean([slope.(ephys_type).run.PC(:,3);slope.(ephys_type).run.FC(:,3);])*log10(freq.(ephys_type))),'color', colorlist(3,:));
set(gca, 'xscale','log', 'yscale','log');
axis square;
pt4 = box_line(freq.(ephys_type), power_norm_ave.(ephys_type).run.brain.DV800,...
    power_norm_sem.(ephys_type).run.brain.DV800, colorlist(4,:)); hold on;
plot(freq.(ephys_type), power_norm_ave.(ephys_type).run.brain.DV800, 'color', colorlist(4,:));
plot(freq.(ephys_type), 10.^(nanmean([slope.(ephys_type).run.PC(:,4);slope.(ephys_type).run.FC(:,4);])*log10(freq.(ephys_type))),'color', colorlist(4,:));
axis(axis_plot.(ephys_type));
set(gca, 'xscale','log', 'yscale','log');
axis square;
title(['all data (n = ',num2str(size(power_norm.(ephys_type).run.brain.DV100,1)),')']);
legend([pt1, pt2, pt3, pt4],...
    {'100 \mum'; '300 \mum'; '500 \mum'; '800 \mum'});
xlabel('Frequency (Hz)');
ylabel('Power (mmHg^2/Hz)');
linkaxes([h1,h2],'x');
if ismember(ephys_type, {'LFP','MUA'})
    savefig(gcf, [fig_filen,'_top']);
    close(gcf);
else
    savefig(gcf, fig_filen);
    close(gcf);
end

%% power-law fit exponent, median +/- IQR
axis_barplot.LFP = [0.4 4.6 0 3];
axis_barplot.Gamma = [0.4 4.6 -0.5 1.2];
axis_barplot.Beta = [0.4 4.6 -0.4 0.7];
axis_barplot.SubAlpha = [0.4 4.6 -0.4 0.9];
axis_barplot.MUA = [0.4 4.6 0 1.1];

x1 = [1 2 3 4];

figure('name',[ephys_type, ' slope of power spectra'],...
    'position',[100 100 1600 400]);
h1 = subplot(121);
plotSpread([-slope.(ephys_type).rest.PC; -slope.(ephys_type).rest.FC;],'xValues',x1,...
    'categoryIdx',repmat([zeros(PC_num,1);ones(FC_num,1);],1,4),...
    'categoryMarkers',{'o','o'},'categoryColors',{'b','m'});
hold on;
boxplot([-slope.(ephys_type).rest.PC;-slope.(ephys_type).rest.FC;],x1,'Width', 0.5,'Color','k','symbol','');
set(findobj(gcf,'-regexp','Tag','\w*Whisker'),'LineStyle','-');
hold on;
plot([x1(1)-0.25,x1(1)+0.25],repmat((nanmean([-slope.(ephys_type).rest.PC(:,1);-slope.(ephys_type).rest.FC(:,1);]))',1,2),'k--');
plot([x1(2)-0.25,x1(2)+0.25],repmat((nanmean([-slope.(ephys_type).rest.PC(:,2);-slope.(ephys_type).rest.FC(:,2);]))',1,2),'k--');
plot([x1(3)-0.25,x1(3)+0.25],repmat((nanmean([-slope.(ephys_type).rest.PC(:,3);-slope.(ephys_type).rest.FC(:,3);]))',1,2),'k--');
plot([x1(4)-0.25,x1(4)+0.25],repmat((nanmean([-slope.(ephys_type).rest.PC(:,4);-slope.(ephys_type).rest.FC(:,4);]))',1,2),'k--');
axis(axis_barplot.(ephys_type));
set(gca,'box','off','xtick',x1, 'xticklabel', {'100 um'; '300 um'; '500 um'; '800 um'});
ylabel('Power-law slope: rest');

h2 = subplot(122);
plotSpread([-slope.(ephys_type).run.PC; -slope.(ephys_type).run.FC;],'xValues',x1,...
    'categoryIdx',repmat([zeros(PC_num,1);ones(FC_num,1);],1,4),...
    'categoryMarkers',{'o','o'},'categoryColors',{'b','m'});
hold on;
boxplot([-slope.(ephys_type).run.PC;-slope.(ephys_type).run.FC;],x1,'Width', 0.5,'Color','k','symbol','');
set(findobj(gcf,'-regexp','Tag','\w*Whisker'),'LineStyle','-');
hold on;
plot([x1(1)-0.25,x1(1)+0.25],repmat((nanmean([-slope.(ephys_type).run.PC(:,1);-slope.(ephys_type).run.FC(:,1);]))',1,2),'k--');
plot([x1(2)-0.25,x1(2)+0.25],repmat((nanmean([-slope.(ephys_type).run.PC(:,2);-slope.(ephys_type).run.FC(:,2);]))',1,2),'k--');
plot([x1(3)-0.25,x1(3)+0.25],repmat((nanmean([-slope.(ephys_type).run.PC(:,3);-slope.(ephys_type).run.FC(:,3);]))',1,2),'k--');
plot([x1(4)-0.25,x1(4)+0.25],repmat((nanmean([-slope.(ephys_type).run.PC(:,4);-slope.(ephys_type).run.FC(:,4);]))',1,2),'k--');
axis(axis_barplot.(ephys_type));
set(gca,'box','off','xtick',x1, 'xticklabel', {'100 um'; '300 um'; '500 um'; '800 um'});
ylabel('Power-law slope: run');

if ismember(ephys_type,{'LFP','MUA'})
    savefig(gcf, [fig_filen,'_bottom']);
    close(gcf);
end
if ismember(ephys_type,{'Gamma','Beta', 'SubAlpha'})
    if strcmp(ephys_type, 'Gamma')
        savefig(gcf, strrep(fig_filen,'_2d','_2e'));
        close(gcf);
    end
    if strcmp(ephys_type, 'Beta')
        savefig(gcf, strrep(fig_filen,'_S3b','_S3d'));
        close(gcf);
    end
    if strcmp(ephys_type, 'SubAlpha')
        savefig(gcf, strrep(fig_filen,'_S3c','_S3e'));
        close(gcf);
    end
end

%% DFA scaling exponent, median +/- IQR
axis_barplot.Gamma = [0.4 4.6 0 1];
axis_barplot.Beta = [0.4 4.6 0 1];
axis_barplot.SubAlpha = [0.4 4.6 0 1.2];

x1 = [1 2 3 4];

figure('name',[ephys_type, ' DFA scaling exponent'],...
    'position',[100 100 1600 400]);
h1 = subplot(121);
plotSpread([DFA.(ephys_type).rest.PC; DFA.(ephys_type).rest.FC;],'xValues',x1,...
    'categoryIdx',repmat([zeros(PC_num,1);ones(FC_num,1);],1,4),...
    'categoryMarkers',{'o','o'},'categoryColors',{'b','m'});
hold on;
boxplot([DFA.(ephys_type).rest.PC;DFA.(ephys_type).rest.FC;],x1,'Width', 0.5,'Color','k','symbol','');
set(findobj(gcf,'-regexp','Tag','\w*Whisker'),'LineStyle','-');
hold on;
plot([x1(1)-0.25,x1(1)+0.25],repmat((nanmean([DFA.(ephys_type).rest.PC(:,1);DFA.(ephys_type).rest.FC(:,1);]))',1,2),'k--');
plot([x1(2)-0.25,x1(2)+0.25],repmat((nanmean([DFA.(ephys_type).rest.PC(:,2);DFA.(ephys_type).rest.FC(:,2);]))',1,2),'k--');
plot([x1(3)-0.25,x1(3)+0.25],repmat((nanmean([DFA.(ephys_type).rest.PC(:,3);DFA.(ephys_type).rest.FC(:,3);]))',1,2),'k--');
plot([x1(4)-0.25,x1(4)+0.25],repmat((nanmean([DFA.(ephys_type).rest.PC(:,4);DFA.(ephys_type).rest.FC(:,4);]))',1,2),'k--');
axis(axis_barplot.(ephys_type));
set(gca,'box','off','xtick',x1, 'xticklabel', {'100 um'; '300 um'; '500 um'; '800 um'});
ylabel('DFA scaling: rest');

h2 = subplot(122);
plotSpread([DFA.(ephys_type).run.PC; DFA.(ephys_type).run.FC;],'xValues',x1,...
    'categoryIdx',repmat([zeros(PC_num,1);ones(FC_num,1);],1,4),...
    'categoryMarkers',{'o','o'},'categoryColors',{'b','m'});
hold on;
boxplot([DFA.(ephys_type).run.PC;DFA.(ephys_type).run.FC;],x1,'Width', 0.5,'Color','k','symbol','');
set(findobj(gcf,'-regexp','Tag','\w*Whisker'),'LineStyle','-');
hold on;
plot([x1(1)-0.25,x1(1)+0.25],repmat((nanmean([DFA.(ephys_type).run.PC(:,1);DFA.(ephys_type).run.FC(:,1);]))',1,2),'k--');
plot([x1(2)-0.25,x1(2)+0.25],repmat((nanmean([DFA.(ephys_type).run.PC(:,2);DFA.(ephys_type).run.FC(:,2);]))',1,2),'k--');
plot([x1(3)-0.25,x1(3)+0.25],repmat((nanmean([DFA.(ephys_type).run.PC(:,3);DFA.(ephys_type).run.FC(:,3);]))',1,2),'k--');
plot([x1(4)-0.25,x1(4)+0.25],repmat((nanmean([DFA.(ephys_type).run.PC(:,4);DFA.(ephys_type).run.FC(:,4);]))',1,2),'k--');
axis(axis_barplot.(ephys_type));
set(gca,'box','off','xtick',x1, 'xticklabel', {'100 um'; '300 um'; '500 um'; '800 um'});ylabel('DFA scaling: run');

if ismember(ephys_type,{'Gamma','Beta', 'SubAlpha'})
    if strcmp(ephys_type, 'Gamma')
        savefig(gcf, strrep(fig_filen,'_2d','_2f'));
        close(gcf);
    end
    if strcmp(ephys_type, 'Beta')
        savefig(gcf, strrep(fig_filen,'_S3b','_S3f'));
        close(gcf);
    end
    if strcmp(ephys_type, 'SubAlpha')
        savefig(gcf, strrep(fig_filen,'_S3c','_S3g'));
        close(gcf);
    end
else
    close(gcf);
end

% >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  STATS for power law exponent
%% power law exponent Stats: layer specific change at rest
slope_rest.(ephys_type) = [slope.(ephys_type).rest.PC;slope.(ephys_type).rest.FC;];
% nanmean(slope_rest.(ephys_type),1)
% nanstd(slope_rest.(ephys_type),[],1)

switch ephys_type
    case 'LFP'
        % first, check if the data is normalized
        normal_distribution = adtest(slope_rest.(ephys_type)(:)); % 0, normal
        % second , check if the data is equal variance
        equal_variance = vartestn(slope_rest.(ephys_type),'display','off'); % p > 0.05, equal variance
        [p, tbl, stats] = anova1(slope_rest.(ephys_type),[1,2,3,4],'off'); % F(3,34) = 0.7794, p = 0.5145
        
    case 'Gamma'
        % first, check if the data is normalized
        normal_distribution = adtest(slope_rest.(ephys_type)(:)); % 0, normal
        % second , check if the data is equal variance
        equal_variance = vartestn(slope_rest.(ephys_type),'display','off'); % p > 0.05, equal variance
        [p, tbl, stats] = anova1(slope_rest.(ephys_type),[1,2,3,4],'off'); % F(3,34) = 0.7794, p = 0.5145
        
    case 'MUA'
        % first, check if the data is normalized
        normal_distribution = adtest(slope_rest.(ephys_type)(:)); % 0, normal
        % second , check if the data is equal variance
        equal_variance = vartestn(slope_rest.(ephys_type),'display','off'); % p > 0.05, equal variance
        [p, tbl, stats] = anova1(slope_rest.(ephys_type),[1,2,3,4],'off'); % F(3,34) = 0.7794, p = 0.5145

    case 'Beta'
        % first, check if the data is normalized
        normal_distribution = adtest(slope_rest.(ephys_type)(:)); % 0, normal
        % second , check if the data is equal variance
        equal_variance = vartestn(slope_rest.(ephys_type),'display','off'); % p > 0.05, equal variance
        [p, tbl, stats] = anova1(slope_rest.(ephys_type),[1,2,3,4],'off');
        
    case 'SubAlpha'
        % first, check if the data is normalized
        normal_distribution = adtest(slope_rest.(ephys_type)(:)); % 0, normal
        % second , check if the data is equal variance
        equal_variance = vartestn(slope_rest.(ephys_type),'display','off'); % p > 0.05, equal variance
        [p, tbl, stats] = anova1(slope_rest.(ephys_type),[1,2,3,4],'off'); 
end


%% power law exponent Stats: layer specific change at run
slope_run.(ephys_type) = [slope.(ephys_type).run.PC;slope.(ephys_type).run.FC;];
% nanmean(slope_run.(ephys_type),1)
% nanstd(slope_run.(ephys_type),[],1)

switch ephys_type
    case 'LFP'
        % first, check if the data is normalized
        normal_distribution = adtest(slope_run.(ephys_type)(:)); % 0, normal
        % second , check if the data is equal variance
        equal_variance = vartestn(slope_run.(ephys_type),'display','off'); % p > 0.05,equal variance
        [p, tbl, stats] = anova1(slope_run.(ephys_type),[1,2,3,4],'off'); 
    
    case 'Gamma'
        % first, check if the data is normalized
        normal_distribution = adtest(slope_run.(ephys_type)(:)); % 0, normal
        % second , check if the data is equal variance
        equal_variance = vartestn(slope_run.(ephys_type),'display','off'); % p > 0.05, equal variance
        [p, tbl, stats] = anova1(slope_run.(ephys_type),[1,2,3,4],'off'); % F(3,34) = 0.9981, p = 0.4067
        
        
    case 'MUA'
        % first, check if the data is normalized
        normal_distribution = adtest(slope_run.(ephys_type)(:)); % 1, not normal
        % second , check if the data is equal variance
        equal_variance = vartestn(slope_run.(ephys_type),'display','off'); % p > 0.05,equal variance 
        % since the data not normal, and they are paired, use
        % Kruskal-Wallis test
        [p, tbl, stats] = kruskalwallis(slope_run.(ephys_type),[1,2,3,4],'off');

    case 'Beta'
        % first, check if the data is normalized
        normal_distribution = adtest(slope_run.(ephys_type)(:)); % 0, normal
        % second , check if the data is equal variance
        equal_variance = vartestn(slope_run.(ephys_type),'display','off'); % p > 0.05, equal variance
        [p, tbl, stats] = anova1(slope_run.(ephys_type),[1,2,3,4],'off'); % F(3,34) = 0.9981, p = 0.4067
        
        
    case 'SubAlpha'
        % first, check if the data is normalized
        normal_distribution = adtest(slope_run.(ephys_type)(:)); % 0, normal
        % second , check if the data is equal variance
        equal_variance = vartestn(slope_run.(ephys_type),'display','off'); % p > 0.05, equal variance
        [p, tbl, stats] = anova1(slope_run.(ephys_type),[1,2,3,4],'off'); % F(3,34) = 0.9981, p = 0.4067
end


%% power law exponent Stats: rest vs run
slope_rest_ave.(ephys_type) = nanmean(slope_rest.(ephys_type),2);
slope_run_ave.(ephys_type) = nanmean(slope_run.(ephys_type),2);
% nanmean(slope_rest_ave.(ephys_type))
% nanstd(slope_rest_ave.(ephys_type))
% nanmean(slope_run_ave.(ephys_type))
% nanstd(slope_run_ave.(ephys_type))

switch ephys_type
    case 'LFP'
        normal_distribution = adtest([slope_rest_ave.(ephys_type)(:);slope_run_ave.(ephys_type)(:);]); % 0, normal
        % second , check if the data is equal variance
        equal_variance = vartest2(slope_rest_ave.(ephys_type)(:),slope_run_ave.(ephys_type)(:)); % 1, not equal variance
        [h,p,ci,stats] = ttest(slope_rest_ave.(ephys_type)(:),slope_run_ave.(ephys_type)(:)); % t(35)=0.9424, P = 0.3525
    
    case 'MUA'
        normal_distribution = adtest([slope_rest_ave.(ephys_type)(:);slope_run_ave.(ephys_type)(:);]); % 0, normal
        % second , check if the data is equal variance
        equal_variance = vartest2(slope_rest_ave.(ephys_type)(:),slope_run_ave.(ephys_type)(:)); % 1, not equal variance
        [h,p,ci,stats] = ttest(slope_rest_ave.(ephys_type)(:),slope_run_ave.(ephys_type)(:)); % t(35)=0.9424, P = 0.3525
        
    case 'Gamma'
        normal_distribution = adtest([slope_rest_ave.(ephys_type)(:);slope_run_ave.(ephys_type)(:);]); % 0, normal
        % second , check if the data is equal variance
        equal_variance = vartest2(slope_rest_ave.(ephys_type)(:),slope_run_ave.(ephys_type)(:)); % 1, not equal variance
       % since the data has no equal variance, and they are not paired, use
        % Wilcoxon rank sum test
        [p, h, stats] = ranksum(slope_rest_ave.(ephys_type)(:),slope_run_ave.(ephys_type)(:)); % p < 0.0001
    case 'Beta'
        normal_distribution = adtest([slope_rest_ave.(ephys_type)(:);slope_run_ave.(ephys_type)(:);]); % 0, normal
        % second , check if the data is equal variance
        equal_variance = vartest2(slope_rest_ave.(ephys_type)(:),slope_run_ave.(ephys_type)(:)); % 1, not equal variance
        [h,p,ci,stats] = ttest(slope_rest_ave.(ephys_type)(:),slope_run_ave.(ephys_type)(:)); % t(35)=0.9424, P = 0.3525
        
    case 'SubAlpha'
        normal_distribution = adtest([slope_rest_ave.(ephys_type)(:);slope_run_ave.(ephys_type)(:);]); % 0, normal
        % second , check if the data is equal variance
        equal_variance = vartest2(slope_rest_ave.(ephys_type)(:),slope_run_ave.(ephys_type)(:)); % 1, not equal variance
        [h,p,ci,stats] = ttest(slope_rest_ave.(ephys_type)(:),slope_run_ave.(ephys_type)(:)); % t(35)=0.9424, P = 0.3525
        
end


% >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>  STATS for DFA scaling
%% DFA scaling  Stats: layer specific change at rest
DFA_rest.(ephys_type) = [DFA.(ephys_type).rest.PC;DFA.(ephys_type).rest.FC;];
% nanmean(DFA_rest.(ephys_type),1)
% nanstd(DFA_rest.(ephys_type),[],1)

switch ephys_type        
    case 'Gamma'
        % first, check if the data is normalized
        normal_distribution = adtest(DFA_rest.(ephys_type)(:)); % 0, normal
        % second , check if the data is equal variance
        equal_variance = vartestn(DFA_rest.(ephys_type),'display','off'); % p > 0.05, equal variance
        [p, tbl, stats] = anova1(DFA_rest.(ephys_type),[1,2,3,4],'off'); % F(3,35) = 0.7474, p = 0.5319
               
    case 'Beta'
        % first, check if the data is normalized
        normal_distribution = adtest(DFA_rest.(ephys_type)(:)); % 0, normal
        % second , check if the data is equal variance
        equal_variance = vartestn(DFA_rest.(ephys_type),'display','off'); % p > 0.05, equal variance
        [p, tbl, stats] = anova1(DFA_rest.(ephys_type),[1,2,3,4],'off'); % F(3,35) = 0.7474, p = 0.5319
       
        
    case 'SubAlpha'
        % first, check if the data is normalized
        normal_distribution = adtest(DFA_rest.(ephys_type)(:)); % 0, normal
        % second , check if the data is equal variance
        equal_variance = vartestn(DFA_rest.(ephys_type),'display','off'); % p > 0.05, equal variance
        [p, tbl, stats] = anova1(DFA_rest.(ephys_type),[1,2,3,4],'off'); % F(3,35) = 0.7474, p = 0.5319
    
end


%% DFA scaling  Stats: layer specific change at run
DFA_run.(ephys_type) = [DFA.(ephys_type).run.PC;DFA.(ephys_type).run.FC;];
% nanmean(DFA_run.(ephys_type),1)
% nanstd(DFA_run.(ephys_type),[],1)

switch ephys_type        
    case 'Gamma'
        % first, check if the data is normalized
        normal_distribution = adtest(DFA_run.(ephys_type)(:)); % 0, normal
        % second , check if the data is equal variance
        equal_variance = vartestn(DFA_run.(ephys_type),'display','off'); % p > 0.05, equal variance
        [p, tbl, stats] = anova1(DFA_run.(ephys_type),[1,2,3,4],'off');% F(3,35) = 0.1844, p = 0.9063
         
    case 'Beta'
        % first, check if the data is normalized
        normal_distribution = adtest(DFA_run.(ephys_type)(:)); % 0, normal
        % second , check if the data is equal variance
        equal_variance = vartestn(DFA_run.(ephys_type),'display','off'); % p > 0.05, equal variance
        [p, tbl, stats] = anova1(DFA_run.(ephys_type),[1,2,3,4],'off');% F(3,35) = 0.1844, p = 0.9063
         
        
    case 'SubAlpha'
        % first, check if the data is normalized
        normal_distribution = adtest(DFA_run.(ephys_type)(:)); % 1, not normal
        % second , check if the data is equal variance
        equal_variance = vartestn(DFA_run.(ephys_type),'display','off'); % p > 0.05, equal variance
        [p, tbl, stats] = kruskalwallis(DFA_run.(ephys_type),[1,2,3,4],'off');         
end


%% DFA scaling  Stats: rest vs run
DFA_rest_ave.(ephys_type) = nanmean(DFA_rest.(ephys_type),2);
DFA_run_ave.(ephys_type) = nanmean(DFA_run.(ephys_type),2);
% nanmean(DFA_rest_ave.(ephys_type))
% nanstd(DFA_rest_ave.(ephys_type))
% nanmean(DFA_run_ave.(ephys_type))
% nanstd(DFA_run_ave.(ephys_type))

switch ephys_type
    case 'Gamma'
        % first, check if the data is normalized
        normal_distribution = adtest([DFA_rest_ave.(ephys_type)(:);DFA_run_ave.(ephys_type)(:);]); % 1, not normal
        % second , check if the data is equal variance
        equal_variance = vartest2(DFA_rest_ave.(ephys_type)(:),DFA_run_ave.(ephys_type)(:)); % 1, not equal variance
        % since the data has no equal variance, and they are not paired, use
        % Wilcoxon rank sum test
        [p, h, stats] = ranksum(DFA_rest_ave.(ephys_type)(:),DFA_run_ave.(ephys_type)(:)); % p < 0.0001
    case 'Beta'
        % first, check if the data is normalized
        normal_distribution = adtest([DFA_rest_ave.(ephys_type)(:);DFA_run_ave.(ephys_type)(:);]); % 0, normal
        % second , check if the data is equal variance
        equal_variance = vartest2(DFA_rest_ave.(ephys_type)(:),DFA_run_ave.(ephys_type)(:)); % 0, equal variance   
        [h,p,ci,stats] = ttest(DFA_rest_ave.(ephys_type)(:),DFA_run_ave.(ephys_type)(:)); % p < 0.0001
    case 'SubAlpha'
        % first, check if the data is normalized
        normal_distribution = adtest([DFA_rest_ave.(ephys_type)(:);DFA_run_ave.(ephys_type)(:);]); % 0, normal
        % second , check if the data is equal variance
        equal_variance = vartest2(DFA_rest_ave.(ephys_type)(:),DFA_run_ave.(ephys_type)(:)); % 0, equal variance
        [h,p,ci,stats] = ttest(DFA_rest_ave.(ephys_type)(:),DFA_run_ave.(ephys_type)(:)); % p < 0.0001

end
end


%% House keeping function
function power_O2_norm = normalized_power(power_O2, num_freq)

power_O2_norm.rest.PC.DV100 = normalized_power_worker(power_O2.rest.PC.DV100, num_freq);
power_O2_norm.rest.PC.DV300 = normalized_power_worker(power_O2.rest.PC.DV300, num_freq);
power_O2_norm.rest.PC.DV500 = normalized_power_worker(power_O2.rest.PC.DV500, num_freq);
power_O2_norm.rest.PC.DV800 = normalized_power_worker(power_O2.rest.PC.DV800, num_freq);

power_O2_norm.rest.FC.DV100 = normalized_power_worker(power_O2.rest.FC.DV100, num_freq);
power_O2_norm.rest.FC.DV300 = normalized_power_worker(power_O2.rest.FC.DV300, num_freq);
power_O2_norm.rest.FC.DV500 = normalized_power_worker(power_O2.rest.FC.DV500, num_freq);
power_O2_norm.rest.FC.DV800 = normalized_power_worker(power_O2.rest.FC.DV800, num_freq);

power_O2_norm.run.PC.DV100 = normalized_power_worker(power_O2.run.PC.DV100, num_freq);
power_O2_norm.run.PC.DV300 = normalized_power_worker(power_O2.run.PC.DV300, num_freq);
power_O2_norm.run.PC.DV500 = normalized_power_worker(power_O2.run.PC.DV500, num_freq);
power_O2_norm.run.PC.DV800 = normalized_power_worker(power_O2.run.PC.DV800, num_freq);

power_O2_norm.run.FC.DV100 = normalized_power_worker(power_O2.run.FC.DV100, num_freq);
power_O2_norm.run.FC.DV300 = normalized_power_worker(power_O2.run.FC.DV300, num_freq);
power_O2_norm.run.FC.DV500 = normalized_power_worker(power_O2.run.FC.DV500, num_freq);
power_O2_norm.run.FC.DV800 = normalized_power_worker(power_O2.run.FC.DV800, num_freq);

% pool PC and FC together
power_O2_norm.rest.brain.DV100 = normalized_power_worker([power_O2.rest.PC.DV100;power_O2.rest.FC.DV100;], num_freq);
power_O2_norm.rest.brain.DV300 = normalized_power_worker([power_O2.rest.PC.DV300;power_O2.rest.FC.DV300;], num_freq);
power_O2_norm.rest.brain.DV500 = normalized_power_worker([power_O2.rest.PC.DV500;power_O2.rest.FC.DV500;], num_freq);
power_O2_norm.rest.brain.DV800 = normalized_power_worker([power_O2.rest.PC.DV800;power_O2.rest.FC.DV800;], num_freq);

power_O2_norm.run.brain.DV100 = normalized_power_worker([power_O2.run.PC.DV100;power_O2.run.FC.DV100;], num_freq);
power_O2_norm.run.brain.DV300 = normalized_power_worker([power_O2.run.PC.DV300;power_O2.run.FC.DV300;], num_freq);
power_O2_norm.run.brain.DV500 = normalized_power_worker([power_O2.run.PC.DV500;power_O2.run.FC.DV500;], num_freq);
power_O2_norm.run.brain.DV800 = normalized_power_worker([power_O2.run.PC.DV800;power_O2.run.FC.DV800;], num_freq);

end

function power_norm = normalized_power_worker(power, num_freq)
TP = nanmean(power,2); % total power
TP = repmat(TP,1,num_freq);
power_norm = power./TP;
end

function [power_O2_ave, power_O2_sem] = group_average_power_O2(power_O2)
% 1. power, rest, PC
power_O2_ave.rest.PC.DV100 = nanmean(power_O2.rest.PC.DV100,1);
power_O2_sem.rest.PC.DV100 = nanstd(power_O2.rest.PC.DV100,[],1)./sqrt(sum(~isnan(power_O2.rest.PC.DV100),1));

power_O2_ave.rest.PC.DV300 = nanmean(power_O2.rest.PC.DV300,1);
power_O2_sem.rest.PC.DV300 = nanstd(power_O2.rest.PC.DV300,[],1)./sqrt(sum(~isnan(power_O2.rest.PC.DV300),1));

power_O2_ave.rest.PC.DV500 = nanmean(power_O2.rest.PC.DV500,1);
power_O2_sem.rest.PC.DV500 = nanstd(power_O2.rest.PC.DV500,[],1)./sqrt(sum(~isnan(power_O2.rest.PC.DV500),1));

power_O2_ave.rest.PC.DV800 = nanmean(power_O2.rest.PC.DV800,1);
power_O2_sem.rest.PC.DV800 = nanstd(power_O2.rest.PC.DV800,[],1)./sqrt(sum(~isnan(power_O2.rest.PC.DV800),1));

% 2. power, run, PC
power_O2_ave.run.PC.DV100 = nanmean(power_O2.run.PC.DV100,1);
power_O2_sem.run.PC.DV100 = nanstd(power_O2.run.PC.DV100,[],1)./sqrt(sum(~isnan(power_O2.run.PC.DV100),1));

power_O2_ave.run.PC.DV300 = nanmean(power_O2.run.PC.DV300,1);
power_O2_sem.run.PC.DV300 = nanstd(power_O2.run.PC.DV300,[],1)./sqrt(sum(~isnan(power_O2.run.PC.DV300),1));

power_O2_ave.run.PC.DV500 = nanmean(power_O2.run.PC.DV500,1);
power_O2_sem.run.PC.DV500 = nanstd(power_O2.run.PC.DV500,[],1)./sqrt(sum(~isnan(power_O2.run.PC.DV500),1));

power_O2_ave.run.PC.DV800 = nanmean(power_O2.run.PC.DV800,1);
power_O2_sem.run.PC.DV800 = nanstd(power_O2.run.PC.DV800,[],1)./sqrt(sum(~isnan(power_O2.run.PC.DV800),1));

% 3. power, rest, FC
power_O2_ave.rest.FC.DV100 = nanmean(power_O2.rest.FC.DV100,1);
power_O2_sem.rest.FC.DV100 = nanstd(power_O2.rest.FC.DV100,[],1)./sqrt(sum(~isnan(power_O2.rest.FC.DV100),1));

power_O2_ave.rest.FC.DV300 = nanmean(power_O2.rest.FC.DV300,1);
power_O2_sem.rest.FC.DV300 = nanstd(power_O2.rest.FC.DV300,[],1)./sqrt(sum(~isnan(power_O2.rest.FC.DV300),1));

power_O2_ave.rest.FC.DV500 = nanmean(power_O2.rest.FC.DV500,1);
power_O2_sem.rest.FC.DV500 = nanstd(power_O2.rest.FC.DV500,[],1)./sqrt(sum(~isnan(power_O2.rest.FC.DV500),1));

power_O2_ave.rest.FC.DV800 = nanmean(power_O2.rest.FC.DV800,1);
power_O2_sem.rest.FC.DV800 = nanstd(power_O2.rest.FC.DV800,[],1)./sqrt(sum(~isnan(power_O2.rest.FC.DV800),1));

% 4. power, run, FC
power_O2_ave.run.FC.DV100 = nanmean(power_O2.run.FC.DV100,1);
power_O2_sem.run.FC.DV100 = nanstd(power_O2.run.FC.DV100,[],1)./sqrt(sum(~isnan(power_O2.run.FC.DV100),1));

power_O2_ave.run.FC.DV300 = nanmean(power_O2.run.FC.DV300,1);
power_O2_sem.run.FC.DV300 = nanstd(power_O2.run.FC.DV300,[],1)./sqrt(sum(~isnan(power_O2.run.FC.DV300),1));

power_O2_ave.run.FC.DV500 = nanmean(power_O2.run.FC.DV500,1);
power_O2_sem.run.FC.DV500 = nanstd(power_O2.run.FC.DV500,[],1)./sqrt(sum(~isnan(power_O2.run.FC.DV500),1));

power_O2_ave.run.FC.DV800 = nanmean(power_O2.run.FC.DV800,1);
power_O2_sem.run.FC.DV800 = nanstd(power_O2.run.FC.DV800,[],1)./sqrt(sum(~isnan(power_O2.run.FC.DV800),1));

% 5. power, rest, brain
power_O2_ave.rest.brain.DV100 = nanmean(power_O2.rest.brain.DV100,1);
power_O2_sem.rest.brain.DV100 = nanstd(power_O2.rest.brain.DV100,[],1)./sqrt(sum(~isnan(power_O2.rest.brain.DV100),1));

power_O2_ave.rest.brain.DV300 = nanmean(power_O2.rest.brain.DV300,1);
power_O2_sem.rest.brain.DV300 = nanstd(power_O2.rest.brain.DV300,[],1)./sqrt(sum(~isnan(power_O2.rest.brain.DV300),1));

power_O2_ave.rest.brain.DV500 = nanmean(power_O2.rest.brain.DV500,1);
power_O2_sem.rest.brain.DV500 = nanstd(power_O2.rest.brain.DV500,[],1)./sqrt(sum(~isnan(power_O2.rest.brain.DV500),1));

power_O2_ave.rest.brain.DV800 = nanmean(power_O2.rest.brain.DV800,1);
power_O2_sem.rest.brain.DV800 = nanstd(power_O2.rest.brain.DV800,[],1)./sqrt(sum(~isnan(power_O2.rest.brain.DV800),1));

% 6. power, run, brain
power_O2_ave.run.brain.DV100 = nanmean(power_O2.run.brain.DV100,1);
power_O2_sem.run.brain.DV100 = nanstd(power_O2.run.brain.DV100,[],1)./sqrt(sum(~isnan(power_O2.run.brain.DV100),1));

power_O2_ave.run.brain.DV300 = nanmean(power_O2.run.brain.DV300,1);
power_O2_sem.run.brain.DV300 = nanstd(power_O2.run.brain.DV300,[],1)./sqrt(sum(~isnan(power_O2.run.brain.DV300),1));

power_O2_ave.run.brain.DV500 = nanmean(power_O2.run.brain.DV500,1);
power_O2_sem.run.brain.DV500 = nanstd(power_O2.run.brain.DV500,[],1)./sqrt(sum(~isnan(power_O2.run.brain.DV500),1));

power_O2_ave.run.brain.DV800 = nanmean(power_O2.run.brain.DV800,1);
power_O2_sem.run.brain.DV800 = nanstd(power_O2.run.brain.DV800,[],1)./sqrt(sum(~isnan(power_O2.run.brain.DV800),1));


end