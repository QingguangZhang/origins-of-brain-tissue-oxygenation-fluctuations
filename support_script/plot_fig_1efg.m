function plot_fig_1efg(rootfolder)
data = load(fullfile(rootfolder,'Data','Oxygen','Oxygen.mat'));
power_O2 = data.power;
slope = data.plaw_slope;
DFA_slope = data.DFA_slope;
freq = data.freq;

PC_num = 23; % 23 mice for oxygen measurement in PC
FC_num = 14; % 14 mice for oxygen measurement in FC

%% oxygen power spectrum: raw data
[power_O2_ave, power_O2_sem] = group_average_power_O2(power_O2); % raw data
axis_raw = [0.01 1 10^-3 10^2.5]; % raw axis

% determine colormap for the following figures
colorlist = cbrewer('qual', 'Set2', 4);

% generate figure
figure('name','oxygen power spectrum at different cortical depths: raw power');
h1 = subplot(221);
pt1 = box_line(freq, power_O2_ave.rest.PC.DV100, power_O2_sem.rest.PC.DV100,  colorlist(1,:)); hold on;
plot(freq, power_O2_ave.rest.PC.DV100, 'color', colorlist(1,:));
set(gca, 'xscale','log', 'yscale','log');
axis square;
pt2 = box_line(freq, power_O2_ave.rest.PC.DV300, power_O2_sem.rest.PC.DV300, colorlist(2,:)); hold on;
plot(freq, power_O2_ave.rest.PC.DV300, 'color', colorlist(2,:));
set(gca, 'xscale','log', 'yscale','log');
axis square;
pt3 = box_line(freq, power_O2_ave.rest.PC.DV500, power_O2_sem.rest.PC.DV500, colorlist(3,:)); hold on;
plot(freq, power_O2_ave.rest.PC.DV500, 'color', colorlist(3,:));
set(gca, 'xscale','log', 'yscale','log');
axis square;
pt4 = box_line(freq, power_O2_ave.rest.PC.DV800, power_O2_sem.rest.PC.DV800, colorlist(4,:)); hold on;
plot(freq, power_O2_ave.rest.PC.DV800, 'color', colorlist(4,:));
axis(axis_raw);
set(gca, 'xscale','log', 'yscale','log');
axis square;
title(['FL/HL, rest (n = ',num2str(size(power_O2.rest.PC.DV100,1)),')']);
legend([pt1, pt2, pt3, pt4],...
    {'100 \mum'; '300 \mum'; '500 \mum'; '800 \mum'});
xlabel('Frequency (Hz)');
ylabel('Power (mmHg^2/Hz)');

h2 = subplot(222);
pt1 = box_line(freq, power_O2_ave.rest.FC.DV100, power_O2_sem.rest.FC.DV100, colorlist(1,:)); hold on;
plot(freq, power_O2_ave.rest.FC.DV100, 'color', colorlist(1,:));
set(gca, 'xscale','log', 'yscale','log');
axis square;
pt2 = box_line(freq, power_O2_ave.rest.FC.DV300, power_O2_sem.rest.FC.DV300, colorlist(2,:)); hold on;
plot(freq, power_O2_ave.rest.FC.DV300, 'color', colorlist(2,:));
set(gca, 'xscale','log', 'yscale','log');
axis square;
pt3 = box_line(freq, power_O2_ave.rest.FC.DV500, power_O2_sem.rest.FC.DV500, colorlist(3,:)); hold on;
plot(freq, power_O2_ave.rest.FC.DV500, 'color', colorlist(3,:));
set(gca, 'xscale','log', 'yscale','log');
axis square;
pt4 = box_line(freq, power_O2_ave.rest.FC.DV800, power_O2_sem.rest.FC.DV800, colorlist(4,:)); hold on;
plot(freq, power_O2_ave.rest.FC.DV800, 'color', colorlist(4,:));
axis(axis_raw);
set(gca, 'xscale','log', 'yscale','log');
axis square;
title(['FC, rest (n = ',num2str(size(power_O2.rest.FC.DV100,1)),')']);
legend([pt1, pt2, pt3, pt4],...
    {'100 \mum'; '300 \mum'; '500 \mum'; '800 \mum'});
xlabel('Frequency (Hz)');
ylabel('Power (mmHg^2/Hz)');

h3 = subplot(223);
pt1 = box_line(freq, power_O2_ave.run.PC.DV100, power_O2_sem.run.PC.DV100, colorlist(1,:)); hold on;
plot(freq, power_O2_ave.run.PC.DV100, 'color', colorlist(1,:));
set(gca, 'xscale','log', 'yscale','log');
axis square;
pt2 = box_line(freq, power_O2_ave.run.PC.DV300, power_O2_sem.run.PC.DV300, colorlist(2,:)); hold on;
plot(freq, power_O2_ave.run.PC.DV300, 'color', colorlist(2,:));
set(gca, 'xscale','log', 'yscale','log');
axis square;
pt3 = box_line(freq, power_O2_ave.run.PC.DV500, power_O2_sem.run.PC.DV500, colorlist(3,:)); hold on;
plot(freq, power_O2_ave.run.PC.DV500, 'color', colorlist(3,:));
set(gca, 'xscale','log', 'yscale','log');
axis square;
pt4 = box_line(freq, power_O2_ave.run.PC.DV800, power_O2_sem.run.PC.DV800, colorlist(4,:)); hold on;
plot(freq, power_O2_ave.run.PC.DV800, 'color', colorlist(4,:));
axis(axis_raw);
set(gca, 'xscale','log', 'yscale','log');
axis square;
title(['FL/HL, all data (n = ',num2str(size(power_O2.run.PC.DV100,1)),')']);
legend([pt1, pt2, pt3, pt4],...
    {'100 \mum'; '300 \mum'; '500 \mum'; '800 \mum'});
xlabel('Frequency (Hz)');
ylabel('Power (mmHg^2/Hz)');

h4 = subplot(224);
pt1 = box_line(freq, power_O2_ave.run.FC.DV100, power_O2_sem.run.FC.DV100, colorlist(1,:)); hold on;
plot(freq, power_O2_ave.run.FC.DV100, 'color', colorlist(1,:));
set(gca, 'xscale','log', 'yscale','log');
axis square;
pt2 = box_line(freq, power_O2_ave.run.FC.DV300, power_O2_sem.run.FC.DV300, colorlist(2,:)); hold on;
plot(freq, power_O2_ave.run.FC.DV300, 'color', colorlist(2,:));
set(gca, 'xscale','log', 'yscale','log');
axis square;
pt3 = box_line(freq, power_O2_ave.run.FC.DV500, power_O2_sem.run.FC.DV500, colorlist(3,:)); hold on;
plot(freq, power_O2_ave.run.FC.DV500, 'color', colorlist(3,:));
set(gca, 'xscale','log', 'yscale','log');
axis square;
pt4 = box_line(freq, power_O2_ave.run.FC.DV800, power_O2_sem.run.FC.DV800, colorlist(4,:)); hold on;
plot(freq, power_O2_ave.run.FC.DV800, 'color', colorlist(4,:));
axis(axis_raw);
set(gca, 'xscale','log', 'yscale','log');
axis square;
title(['FC, all data (n = ',num2str(size(power_O2.run.FC.DV100,1)),')']);
legend([pt1, pt2, pt3, pt4],...
    {'100 \mum'; '300 \mum'; '500 \mum'; '800 \mum'});
xlabel('Frequency (Hz)');
ylabel('Power (mmHg^2/Hz)');

linkaxes([h1,h2,h3,h4],'x');
savefig(gcf, fullfile(rootfolder, 'Figure', 'Fig_1e'));
close(gcf);

%% power-law fit exponent, median +/- IQR
x1 = [1 2 3 4];
figure('name','Slope of oxygen power spectra',...
    'position',[100 100 1600 400]);
hold on;
h1 = subplot(121);
plotSpread(h1,[-slope.rest.PC; -slope.rest.FC;],'xValues',x1,...
    'categoryIdx',repmat([zeros(PC_num,1);ones(FC_num,1);],1,4),...
    'categoryMarkers',{'o','o'},'categoryColors',{'b','m'});
hold on;
boxplot([-slope.rest.PC;-slope.rest.FC;],x1,'Width', 0.5,'Color','k','symbol','');
set(findobj(gcf,'-regexp','Tag','\w*Whisker'),'LineStyle','-')
hold on;
plot([x1(1)-0.25,x1(1)+0.25],repmat((nanmean([-slope.rest.PC(:,1);-slope.rest.FC(:,1);]))',1,2),'k--');
plot([x1(2)-0.25,x1(2)+0.25],repmat((nanmean([-slope.rest.PC(:,2);-slope.rest.FC(:,2);]))',1,2),'k--');
plot([x1(3)-0.25,x1(3)+0.25],repmat((nanmean([-slope.rest.PC(:,3);-slope.rest.FC(:,3);]))',1,2),'k--');
plot([x1(4)-0.25,x1(4)+0.25],repmat((nanmean([-slope.rest.PC(:,4);-slope.rest.FC(:,4);]))',1,2),'k--');
axis([0.4 4.6 0 2.5]);
set(gca,'box','off','xtick',x1, 'xticklabel', {'100 um'; '300 um'; '500 um'; '800 um'});
ylabel('Power-law slope');
title('Rest');

h2 = subplot(122);
plotSpread([-slope.run.PC; -slope.run.FC;],'xValues',x1,...
    'categoryIdx',repmat([zeros(PC_num,1);ones(FC_num,1);],1,4),...
    'categoryMarkers',{'o','o'},'categoryColors',{'b','m'});
hold on;
boxplot([-slope.run.PC;-slope.run.FC;],x1,'Width',0.5,'Color','k','symbol','');
set(findobj(gcf,'-regexp','Tag','\w*Whisker'),'LineStyle','-')
hold on;
plot([x1(1)-0.25,x1(1)+0.25],repmat((nanmean([-slope.run.PC(:,1);-slope.run.FC(:,1);]))',1,2),'k--');
plot([x1(2)-0.25,x1(2)+0.25],repmat((nanmean([-slope.run.PC(:,2);-slope.run.FC(:,2);]))',1,2),'k--');
plot([x1(3)-0.25,x1(3)+0.25],repmat((nanmean([-slope.run.PC(:,3);-slope.run.FC(:,3);]))',1,2),'k--');
plot([x1(4)-0.25,x1(4)+0.25],repmat((nanmean([-slope.run.PC(:,4);-slope.run.FC(:,4);]))',1,2),'k--');
axis([0.4 4.6 0 2.5]);
set(gca,'box','off','xtick',x1, 'xticklabel', {'100 um'; '300 um'; '500 um'; '800 um'});
ylabel('Power-law slope');
title('All data');

savefig(gcf, fullfile(rootfolder, 'Figure', 'Fig_1f'));
close(gcf);


%% scatter plot showing the relationship between rest vs. run
slope_rest = [slope.rest.PC;slope.rest.FC;]; % slope for all resting data
slope_run = [slope.run.PC;slope.run.FC;];

slope_rest_animal = nanmean(slope_rest,2); % average across layer
slope_run_animal = nanmean(slope_run,2);

% nanmean(slope_rest)
% nanstd(slope_rest)
% 
% nanmean(slope_run)
% nanstd(slope_run)

% nanmean(slope_rest_animal)
% nanstd(slope_rest_animal)
% 
% nanmean(slope_run_animal)
% nanstd(slope_run_animal)

% check slope at rest
% first, check if the data is normalized
normal_distribution = adtest(slope_rest(:)); % 0, normal
% second , check if the data is equal variance
equal_variance = vartestn(slope_rest,'display','off'); % p>0.05, equal variance
[p, tbl, stats] = anova1(slope_rest,[1,2,3,4],'off'); % F(3,143) = 1.9606, p = 0.1227
disp(['No differences of slope among cortical depths at rest, p = ', num2str(p), ' One-way ANOVA']);

% check slope during running
% first, check if the data is normalized
normal_distribution = adtest(slope_run(:)); % 0, normal
% second , check if the data is equal variance
equal_variance = vartestn(slope_run,'display','off'); % p>0.05, equal variance
[p, tbl, stats] = anova1(slope_run,[1,2,3,4],'off'); % F(3,147) = 0.3555, p = 0.7852
disp(['No differences of slope among cortical depths using all data, p = ', num2str(p), ' One-way ANOVA']);

figure('name','Relationship of power law exponent between rest and run');
plot(-slope.rest.FC(:),-slope.run.FC(:), 'marker','o','markersize',10,...
    'markeredgecolor','k','markerfacecolor',[0.91 0.41 0.17],'linestyle','none'); hold on; 
plot(-slope.rest.PC(:),-slope.run.PC(:),'marker','o','markersize',10,...
    'markeredgecolor','k','markerfacecolor',[0.5 0.5 0.5],'linestyle','none');
plot([0 2.5],[0 2.5])
axis([0.3 2.4 0.3 2.4]);
axis square;
xlabel('1/f exponent: rest'); 
ylabel('1/f exponent: all data');
legend({'FC','FL/HL'});
savefig(gcf, fullfile(rootfolder, 'Figure', 'Fig_1h'));
close(gcf);


% Compare slope: rest vs run
% first, check if the data is normalized
normal_distribution = adtest([slope_rest_animal;slope_run_animal;]); % 0, normal
% second , check if the data is equal variance
equal_variance = vartest2(slope_rest_animal, slope_run_animal); % 1, not equal variance
% since the data has no equal variance, and they are paired, use
% Wilcoxon signed rank test
[p, h, stats] = signrank(slope_rest_animal,slope_run_animal); % P < 0.0001
disp('power law slope: rest vs run, p < 0.0001, Wilcoxon signed rank test');

%% compared btwn. brain regions
FC_rest = nanmean(slope.rest.FC,2); % average across all  depths
FC_rest_ave = nanmean(FC_rest);
FC_rest_std = nanstd(FC_rest);

PC_rest = nanmean(slope.rest.PC,2); % average across all  depths
PC_rest_ave = nanmean(PC_rest);
PC_rest_std = nanstd(PC_rest);

% slope: FC vs PC
% first, check if the data is normalized
normal_distribution = adtest([FC_rest;PC_rest;]); % 0, normal
% second , check if the data is equal variance
equal_variance = vartest2(FC_rest, PC_rest); % 0, equal variance
[h,p,ci,stats] = ttest2(FC_rest,PC_rest);
disp(['No difference between fitted exponent in FC vs FL/HL: p = ', num2str(p), ', Wilcoxon signed rank test']);

%% ++++++++++++++++++++++ DFA results
%% DFA exponent, mean +/- IQR
x1 = [1 2 3 4];
figure('name','Slope of oxygen DFA analysis',...
    'position',[100 100 1600 400]);
h1 = subplot(121);
plotSpread([DFA_slope.rest.PC; DFA_slope.rest.FC;],'xValues',x1,...
    'categoryIdx',repmat([zeros(PC_num,1);ones(FC_num,1);],1,4),...
    'categoryMarkers',{'o','o'},'categoryColors',{'b','m'});
hold on;
boxplot([DFA_slope.rest.PC;DFA_slope.rest.FC;],x1,'Width',0.5,'Color','k','symbol','');
set(findobj(gcf,'-regexp','Tag','\w*Whisker'),'LineStyle','-');
hold on;
plot([x1(1)-0.25,x1(1)+0.25],repmat((nanmean([DFA_slope.rest.PC(:,1);DFA_slope.rest.FC(:,1);]))',1,2),'k--');
plot([x1(2)-0.25,x1(2)+0.25],repmat((nanmean([DFA_slope.rest.PC(:,2);DFA_slope.rest.FC(:,2);]))',1,2),'k--');
plot([x1(3)-0.25,x1(3)+0.25],repmat((nanmean([DFA_slope.rest.PC(:,3);DFA_slope.rest.FC(:,3);]))',1,2),'k--');
plot([x1(4)-0.25,x1(4)+0.25],repmat((nanmean([DFA_slope.rest.PC(:,4);DFA_slope.rest.FC(:,4);]))',1,2),'k--');
axis([0.4 4.6 0 1.8]);
set(gca,'box','off','xtick',x1, 'xticklabel', {'100 um'; '300 um'; '500 um'; '800 um'});
ylabel('DFA scaling slope');
title('Rest');

h2 = subplot(122);
plotSpread([DFA_slope.run.PC; DFA_slope.run.FC;],'xValues',x1,...
    'categoryIdx',repmat([zeros(PC_num,1);ones(FC_num,1);],1,4),...
    'categoryMarkers',{'o','o'},'categoryColors',{'b','m'});
hold on;
boxplot([DFA_slope.run.PC;DFA_slope.run.FC;],x1,'Width',0.5,'Color','k','symbol','');
set(findobj(gcf,'-regexp','Tag','\w*Whisker'),'LineStyle','-');
hold on;
plot([x1(1)-0.25,x1(1)+0.25],repmat((nanmean([DFA_slope.run.PC(:,1);DFA_slope.run.FC(:,1);]))',1,2),'k--');
plot([x1(2)-0.25,x1(2)+0.25],repmat((nanmean([DFA_slope.run.PC(:,2);DFA_slope.run.FC(:,2);]))',1,2),'k--');
plot([x1(3)-0.25,x1(3)+0.25],repmat((nanmean([DFA_slope.run.PC(:,3);DFA_slope.run.FC(:,3);]))',1,2),'k--');
plot([x1(4)-0.25,x1(4)+0.25],repmat((nanmean([DFA_slope.run.PC(:,4);DFA_slope.run.FC(:,4);]))',1,2),'k--');
axis([0.4 4.6 0 1.8]);
set(gca,'box','off','xtick',x1, 'xticklabel', {'100 um'; '300 um'; '500 um'; '800 um'});
ylabel('DFA scaling slope');
title('All data');

savefig(gcf, fullfile(rootfolder, 'Figure', 'Fig_1g'));
close(gcf);


%% DFA stats
DFA_slope_rest = [DFA_slope.rest.PC;DFA_slope.rest.FC;]; % slope for all resting data
DFA_slope_run = [DFA_slope.run.PC;DFA_slope.run.FC;];

DFA_slope_rest_animal = nanmean(DFA_slope_rest,2); % average across layer
DFA_slope_run_animal = nanmean(DFA_slope_run,2);

% nanmean(DFA_slope_rest)
% nanstd(DFA_slope_rest)
% 
% nanmean(DFA_slope_run)
% nanstd(DFA_slope_run)

% nanmean(DFA_slope_rest_animal)
% nanstd(DFA_slope_rest_animal)
% 
% nanmean(DFA_slope_run_animal)
% nanstd(DFA_slope_run_animal)

% check DFA_slope at rest
% first, check if the data is normalized
normal_distribution = adtest(DFA_slope_rest(:)); % 1, not normal
% second , check if the data is equal variance
equal_variance = vartestn(DFA_slope_rest,'display','off'); % p>0.05, equal variance
% since the data has no equal variance, and they are paired, use
% Kruskal-Wallis test
[p, tbl, stats] = kruskalwallis(DFA_slope_rest,[1,2,3,4],'off'); % chi-square(3,143)=13.5451, p = 0.0036
disp(['No differences of DFA slope among cortical depths at rest, p = ', num2str(p), ' Kruskal-Wallis test']);

% check DFA_slope during running
% first, check if the data is normalized
normal_distribution = adtest(DFA_slope_run(:)); % 1, not normal
% second , check if the data is equal variance
equal_variance = vartestn(DFA_slope_run,'display','off'); % p>0.05, equal variance
% % % % since the data has no equal variance, and they are paired, use
% % % % Kruskal-Wallis test
[p, tbl, stats] = kruskalwallis(DFA_slope_run,[1,2,3,4],'off'); % chi-square(3,147)=4.5401, p = 0.2087


% Compare DFA_slope: rest vs run
% first, check if the data is normalized
normal_distribution = adtest([DFA_slope_rest_animal;DFA_slope_run_animal;]); % 0, normal
% second , check if the data is equal variance
equal_variance = vartest2(DFA_slope_rest_animal, DFA_slope_run_animal); % 1, not equal variance
% since the data has no equal variance, and they are paired, use
% Wilcoxon signed rank test
[p, h, stats] = signrank(DFA_slope_rest_animal,DFA_slope_run_animal); % P < 0.0001
disp('DFA slope: rest vs run, p < 0.0001, Wilcoxon signed rank test');


% compared btwn. brain regions
FC_rest = nanmean(DFA_slope.rest.FC,2); % average across all  depths
FC_rest_ave = nanmean(FC_rest);
FC_rest_std = nanstd(FC_rest);

PC_rest = nanmean(DFA_slope.rest.PC,2); % average across all  depths
PC_rest_ave = nanmean(PC_rest);
PC_rest_std = nanstd(PC_rest);

% DFA_slope: FC vs PC
% first, check if the data is normalized
normal_distribution = adtest([FC_rest;PC_rest;]); % 0, normal
% second , check if the data is equal variance
equal_variance = vartest2(FC_rest, PC_rest); % 0, equal variance
[h,p,ci,stats] = ttest2(FC_rest,PC_rest);


end

function [power_O2_ave, power_O2_sem] = group_average_power_O2(power_O2)
% 1. power, rest, PC
power_O2_ave.rest.PC.DV100 = nanmean(power_O2.rest.PC.DV100,1);
power_O2_sem.rest.PC.DV100 = nanstd(power_O2.rest.PC.DV100,[],1)/sqrt(length(~isnan(power_O2.rest.PC.DV100(:,1))));

power_O2_ave.rest.PC.DV300 = nanmean(power_O2.rest.PC.DV300,1);
power_O2_sem.rest.PC.DV300 = nanstd(power_O2.rest.PC.DV300,[],1)/sqrt(length(~isnan(power_O2.rest.PC.DV300(:,1))));

power_O2_ave.rest.PC.DV500 = nanmean(power_O2.rest.PC.DV500,1);
power_O2_sem.rest.PC.DV500 = nanstd(power_O2.rest.PC.DV500,[],1)/sqrt(length(~isnan(power_O2.rest.PC.DV500(:,1))));

power_O2_ave.rest.PC.DV800 = nanmean(power_O2.rest.PC.DV800,1);
power_O2_sem.rest.PC.DV800 = nanstd(power_O2.rest.PC.DV800,[],1)/sqrt(length(~isnan(power_O2.rest.PC.DV800(:,1))));

% 2. power, run, PC
power_O2_ave.run.PC.DV100 = nanmean(power_O2.run.PC.DV100,1);
power_O2_sem.run.PC.DV100 = nanstd(power_O2.run.PC.DV100,[],1)/sqrt(length(~isnan(power_O2.run.PC.DV100(:,1))));

power_O2_ave.run.PC.DV300 = nanmean(power_O2.run.PC.DV300,1);
power_O2_sem.run.PC.DV300 = nanstd(power_O2.run.PC.DV300,[],1)/sqrt(length(~isnan(power_O2.run.PC.DV300(:,1))));

power_O2_ave.run.PC.DV500 = nanmean(power_O2.run.PC.DV500,1);
power_O2_sem.run.PC.DV500 = nanstd(power_O2.run.PC.DV500,[],1)/sqrt(length(~isnan(power_O2.run.PC.DV500(:,1))));

power_O2_ave.run.PC.DV800 = nanmean(power_O2.run.PC.DV800,1);
power_O2_sem.run.PC.DV800 = nanstd(power_O2.run.PC.DV800,[],1)/sqrt(length(~isnan(power_O2.run.PC.DV800(:,1))));

% 3. power, rest, FC
power_O2_ave.rest.FC.DV100 = nanmean(power_O2.rest.FC.DV100,1);
power_O2_sem.rest.FC.DV100 = nanstd(power_O2.rest.FC.DV100,[],1)/sqrt(length(~isnan(power_O2.rest.FC.DV100(:,1))));

power_O2_ave.rest.FC.DV300 = nanmean(power_O2.rest.FC.DV300,1);
power_O2_sem.rest.FC.DV300 = nanstd(power_O2.rest.FC.DV300,[],1)/sqrt(length(~isnan(power_O2.rest.FC.DV300(:,1))));

power_O2_ave.rest.FC.DV500 = nanmean(power_O2.rest.FC.DV500,1);
power_O2_sem.rest.FC.DV500 = nanstd(power_O2.rest.FC.DV500,[],1)/sqrt(length(~isnan(power_O2.rest.FC.DV500(:,1))));

power_O2_ave.rest.FC.DV800 = nanmean(power_O2.rest.FC.DV800,1);
power_O2_sem.rest.FC.DV800 = nanstd(power_O2.rest.FC.DV800,[],1)/sqrt(length(~isnan(power_O2.rest.FC.DV800(:,1))));

% 4. power, run, FC
power_O2_ave.run.FC.DV100 = nanmean(power_O2.run.FC.DV100,1);
power_O2_sem.run.FC.DV100 = nanstd(power_O2.run.FC.DV100,[],1)/sqrt(length(~isnan(power_O2.run.FC.DV100(:,1))));

power_O2_ave.run.FC.DV300 = nanmean(power_O2.run.FC.DV300,1);
power_O2_sem.run.FC.DV300 = nanstd(power_O2.run.FC.DV300,[],1)/sqrt(length(~isnan(power_O2.run.FC.DV300(:,1))));

power_O2_ave.run.FC.DV500 = nanmean(power_O2.run.FC.DV500,1);
power_O2_sem.run.FC.DV500 = nanstd(power_O2.run.FC.DV500,[],1)/sqrt(length(~isnan(power_O2.run.FC.DV500(:,1))));

power_O2_ave.run.FC.DV800 = nanmean(power_O2.run.FC.DV800,1);
power_O2_sem.run.FC.DV800 = nanstd(power_O2.run.FC.DV800,[],1)/sqrt(length(~isnan(power_O2.run.FC.DV800(:,1))));

end


function [power_O2_ave, power_O2_sem] = group_average_power_O2_diff(power_O2)
% 1. power, rest, PC
power_O2_ave.PC.DV100 = nanmean(power_O2.PC.DV100,1);
power_O2_sem.PC.DV100 = nanstd(power_O2.PC.DV100,[],1)/sqrt(length(~isnan(power_O2.PC.DV100(:,1))));

power_O2_ave.PC.DV300 = nanmean(power_O2.PC.DV300,1);
power_O2_sem.PC.DV300 = nanstd(power_O2.PC.DV300,[],1)/sqrt(length(~isnan(power_O2.PC.DV300(:,1))));

power_O2_ave.PC.DV500 = nanmean(power_O2.PC.DV500,1);
power_O2_sem.PC.DV500 = nanstd(power_O2.PC.DV500,[],1)/sqrt(length(~isnan(power_O2.PC.DV500(:,1))));

power_O2_ave.PC.DV800 = nanmean(power_O2.PC.DV800,1);
power_O2_sem.PC.DV800 = nanstd(power_O2.PC.DV800,[],1)/sqrt(length(~isnan(power_O2.PC.DV800(:,1))));

% 3. power, rest, FC
power_O2_ave.FC.DV100 = nanmean(power_O2.FC.DV100,1);
power_O2_sem.FC.DV100 = nanstd(power_O2.FC.DV100,[],1)/sqrt(length(~isnan(power_O2.FC.DV100(:,1))));

power_O2_ave.FC.DV300 = nanmean(power_O2.FC.DV300,1);
power_O2_sem.FC.DV300 = nanstd(power_O2.FC.DV300,[],1)/sqrt(length(~isnan(power_O2.FC.DV300(:,1))));

power_O2_ave.FC.DV500 = nanmean(power_O2.FC.DV500,1);
power_O2_sem.FC.DV500 = nanstd(power_O2.FC.DV500,[],1)/sqrt(length(~isnan(power_O2.FC.DV500(:,1))));

power_O2_ave.FC.DV800 = nanmean(power_O2.FC.DV800,1);
power_O2_sem.FC.DV800 = nanstd(power_O2.FC.DV800,[],1)/sqrt(length(~isnan(power_O2.FC.DV800(:,1))));

end