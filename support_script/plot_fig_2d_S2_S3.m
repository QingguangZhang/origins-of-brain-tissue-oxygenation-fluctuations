function plot_fig_2d_S2_S3(rootfolder)
data = load(fullfile(rootfolder,'Data','Ephys', 'LaminarEphys.mat'));

power = data.Power;
DFA = data.DFA;
slope = data.slope;
freq = data.freq;


PC_num = 6; % 6 measurement sites in PC
FC_num = 3; % 3 measurement sites in FC

num_freq.LFP = 6488; % for raw LFP signal
num_freq.MUA = 6488; % for raw MUA signal
num_freq.Gamma = 102; % for Gamma-band LFP signal
num_freq.Beta = 102; % for Beta-band LFP signal
num_freq.SubAlpha = 102; % for SubAlpha-band LFP signal


fig_filen = fullfile(rootfolder, 'Figure', 'Fig_2d');
plot_laminar_slope_LFP(freq, power, slope, num_freq, DFA, 'Gamma', PC_num, FC_num, fig_filen);

fig_filen = fullfile(rootfolder, 'Figure', 'Fig_S2d');
plot_laminar_slope_LFP(freq, power, slope, num_freq, DFA, 'LFP', PC_num, FC_num, fig_filen);

fig_filen = fullfile(rootfolder, 'Figure', 'Fig_S2e');
plot_laminar_slope_LFP(freq, power, slope, num_freq, DFA, 'MUA', PC_num, FC_num, fig_filen);

fig_filen = fullfile(rootfolder, 'Figure', 'Fig_S3b');
plot_laminar_slope_LFP(freq, power, slope, num_freq, DFA, 'Beta', PC_num, FC_num, fig_filen);

fig_filen = fullfile(rootfolder, 'Figure', 'Fig_S3c');
plot_laminar_slope_LFP(freq, power, slope, num_freq, DFA, 'SubAlpha', PC_num, FC_num, fig_filen);

%% figure S2bc
data_example = load(fullfile(rootfolder,'Data','Ephys', 'Ephys_example.mat'));
data_example = data_example.out;
LFP = data_example.LFP;
MUA = data_example.MUA;
time = (0:size(LFP,2)-1)/20000;

figure;
subplot(421); plot(time, LFP(1,:)); title('100 \mum');
subplot(423); plot(time, LFP(2,:)); title('300 \mum');
subplot(425); plot(time, LFP(3,:)); title('5100 \mum');
subplot(427); plot(time, LFP(4,:)); title('800 \mum');
xlabel('Time (s)');

subplot(422); plot(time, MUA(1,:)); title('100 \mum');
subplot(424); plot(time, MUA(2,:)); title('300 \mum');
subplot(426); plot(time, MUA(3,:)); title('500 \mum');
subplot(428); plot(time, MUA(4,:)); title('800 \mum');
xlabel('Time (s)');
savefig(gcf, fullfile(rootfolder, 'Figure', 'Fig_S2bc'));
close(gcf);

end
