function out = DFA(data,DFAorder,segments)
% pre-allocate data
timeScale=[];Fn=[];windowNum=0;

%% pre-process the dataset
% remove segments < 5 points
segLength = segments(:,2) - segments(:,1)+1; % data length for each segment
shortSeg = (segLength < 5); % indentify short segments
segments(shortSeg, :) = []; % remove short segments
segLength(shortSeg) = [];
segNum = size(segments,1); % how many long segments

% pick all data points we intend to use
pointsUsed = []; % points used
for segIdx = 1:segNum
    pointsUsed = [pointsUsed, segments(segIdx,1):segments(segIdx,2)];
end
pointsUsed = pointsUsed(:);

x = 1:length(data); % sample numbers
y(x) = NaN; % selected data segments
yi(x) = NaN; % summed and mean-subtracted data
y(pointsUsed) = data(pointsUsed); % used data points

out.pointsUsed = pointsUsed;
out.std_seg = nanstd(y);

%% Work on selected data segments: remove mean and integrate over each segment
for segIdx = 1:segNum % work separately on each segment
    segy = y(segments(segIdx,1):segments(segIdx,2));
    segAve = mean(segy); % mean for each segment
    
    segy = segy-segAve; % mean subtraction
    segSum = cumsum(segy); % integration
    yi(segments(segIdx,1):segments(segIdx,2)) = segSum; % summed and mean-subtracted data
end

%% Select the time scales
minScale = 30; %DFAorder+3; %30 % DFAorder+3, minimum 1 second data, i.e., 1 Hz
maxScale = 1800; % cover 60 s data, i.e., 0.016 Hz
%max(segLength); % maximum seglength, consider replace it with other numbers
minLogScale = log(minScale);
maxLogScale = log(maxScale);
step = log(2)/10;
logScale = minLogScale:step:maxLogScale;
dScale = exp(logScale); % Hurst exponent?
timeScale = floor(dScale); % make it integrate number
num = length(timeScale);

% remove duplicated time scale
i = 2;
while i<=num
    if timeScale(i) == timeScale(i-1)
        timeScale(i) = [];
        num = num-1;
    else
        i = i+1;
    end      
end
    
%% Polynominal fit for each time scale
Fn(1:num)=NaN;windowNum(1:num)=NaN;
for scaleIdx=1:num
    currentScale=timeScale(scaleIdx); % current time scale
    [Fn(scaleIdx),~,~,windowNum(scaleIdx)]=...
                            DFAeachScale(yi,currentScale,segments,DFAorder);
end
% remove NaNs
nanIdx=find(isnan(Fn));
Fn(nanIdx)=[];timeScale(nanIdx)=[];windowNum(nanIdx)=[];

%% Obtain the scaling exponent alpha 
ii=find(timeScale>=6 & windowNum>=4); % select 
logFn=log(Fn(ii));
logtimeScale=log(timeScale(ii));
[coefficient,cerror]=polyfit(logtimeScale,logFn,1); % x-axis: time scale; y-axis: DFA
logfit=coefficient(1)*logtimeScale+coefficient(2); % fitted line in log scale
fit=exp(logfit); % fitted curve in normal scale

out.Fn = Fn;
out.timeScale = timeScale;
out.logFn = logFn;
out.logtimeScale = logtimeScale;
out.coefficient = coefficient;
out.logfit = logfit;

end

function [Fn,DFAtrend,DFAfluctuation,totalWinNum]=DFAeachScale(yi,s,segments,DFAorder)
% yi: a sample time series, summed and mean-subtracted
% s: the length of windows for estimating local trend
% segments: selected data segments for this dataset
% DFAorder: order for polynominal fit to estimate local trend

minWinNum = 4;
Fn=NaN; % for a given window size n, the fluctuation Fn
segNum = size(segments,1);
len=length(yi);
dfatrend=[];dfatrend(1:len)=nan;%polynomial trend 
dfafluctuation=[];dfafluctuation(1:len)=nan;%DFA fluctuation

totalWinNum = 0; % total window numbers
for segIdx = 1:segNum
    N=segments(segIdx,2)-segments(segIdx,1)+1; % sample numbers for each segment
    numWin = floor(N/s); % number of windows (equal length, non-overlapping windows) to estimate local trend
    st = segments(segIdx,1):s:segments(segIdx,2); % start point for each window
    fin = st+s-1; % final point for each window
    if length(st)>numWin
        st(numWin+1:end)=[];
        fin(numWin+1:end)=[];
    end
    
    for windowIdx =1:numWin % for each window, estimate local trend
        data=yi(st(windowIdx):fin(windowIdx)); % work on summed and mean-subtracted data
        x0=st(windowIdx):fin(windowIdx);
        x=(x0-mean(x0))/std(x0);
        p=polyfit(x,data,DFAorder); % polynominal fit 
        yfit=polyval(p,x); % fitted curve,i.e., local trend
        DFAtrend(st(windowIdx):fin(windowIdx))=yfit; % local trend
        DFAfluctuation(st(windowIdx):fin(windowIdx))=yi(st(windowIdx):fin(windowIdx))-yfit; % local fluctuations
    end
    totalWinNum = totalWinNum + numWin; % update total window numbers
end

if totalWinNum >= minWinNum
    Fn=sqrt(nanmean(DFAfluctuation.*DFAfluctuation)); % average across windows to estimate local fluctuation
end

end
