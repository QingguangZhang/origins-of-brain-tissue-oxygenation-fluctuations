function plot_fig_3(rootfolder)
data = load(fullfile(rootfolder,'Data','Oxygen','Coherence.mat'));

freq = data.freq; 
coh_Gamma=data.coh_Gamma;
coh_Beta= data.coh_Beta;
coh_SubAlpha = data.coh_SubAlpha; 
coh_resp = data.coh_resp;
coh_ci_Gamma = data.coh_ci_Gamma; 
coh_ci_Beta = data.coh_ci_Beta; 
coh_ci_SubAlpha = data.coh_ci_SubAlpha;
coh_ci_resp = data.coh_ci_resp; 

%% coherence btwn BLP, resp rate and oxygen
plot_coh_BLP_Resp_O2(freq, coh_Gamma, coh_Beta, coh_SubAlpha, coh_resp, ...
    coh_ci_Gamma, coh_ci_Beta, coh_ci_SubAlpha,coh_ci_resp, rootfolder);
savefig(gcf, fullfile(rootfolder, 'Figure', 'Fig_3'));
close(gcf);
end