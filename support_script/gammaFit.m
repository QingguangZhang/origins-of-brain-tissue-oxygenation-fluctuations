function [params, HRF_gamma, HRF_gamma1, HRF_gamma2, R2, x0] = gammaFit(HRF,HRF_time, option,x0)
    % gammaFit: use gamma function to fit the HRF
    % 
    % INPUTS:
    %       HRF: numerically computed hemodynanic response function
    %       option: constrained (fminsearchbnd) or unconstrained (fminsearch) fitting methods
    %
    % OUTPUTS:
    %       params: fitting parameters
    %       HRF_gamma: gamma function fitting
    
%     x0 = [1 0.2 1 1 1 5]; % initial guess, [A1, A2, T1, T2, W1, W2]

    
    % Initialize option parameters
    options = optimset('Display','none',...      % display no output
                        'FunValCheck','off',...  % check objective values
                        'MaxFunEvals', 2000,...  % max number of function evaluations allowed
                        'MaxIter', 2000,...      % max number of iteration allowed
                        'TolFun',1e-8,...        % termination tolerance on the function value
                        'TolX',1e-8,...          % termination tolerance on x
                        'UseParallel','always'); % always use parallel computation
   
    switch option
        case 'unconstrained'                          
            % unconstrained fitting
            disp('Fitting raw data with two gamma functions: unconstrained');
            params = fminsearch(@(x) impest_gamma(HRF,x,length(HRF_time),HRF_time), x0,options);
            
        case 'constrained'               
            % constrained fitting using fminsearchbnd (downloaded from FileExchange)
            disp('Fitting raw data with two gamma functions: constrained (fminsearchbnd)');
            params = fminsearchbnd(@(x) impest_gamma(HRF,x,length(HRF_time),HRF_time),...
                                    x0, ...
                                    [0, 0, 0, 0, 0, 0], ... % lower bound
                                    [Inf, Inf, Inf, Inf,Inf,Inf],... % upper bound
                                    options);
                                
            % we try to fit with one positive gamma function and one negative gamma
            % function, W (full width at half maximum) and T (time to peak) should all be positive numbers
        otherwise
            error('gammaFit: not the right fitting method');
    end
                        
    % compute HRF estimation using gamma kernel parameters
    A = params(1:2); % amplitude
    T = params(3:4); % time to peak
    W = params(5:6); % full width at half maximum
    
    alpha1 = (T(1)/W(1))^2*8.0*log(2.0);
    beta1 = W(1)^2/(T(1)*8.0*log(2.0));
    
    alpha2 = (T(2)/W(2))^2*8.0*log(2.0);
    beta2 = W(2)^2/(T(2)*8.0*log(2.0));
    
    HRF_gamma1 = A(1)*(HRF_time/T(1)).^alpha1.*exp((HRF_time-T(1))/(-beta1));
    HRF_gamma2 = - A(2)*(HRF_time/T(2)).^alpha2.*exp((HRF_time-T(2))/(-beta2));
    
    HRF_gamma = A(1)*(HRF_time/T(1)).^alpha1.*exp((HRF_time-T(1))/(-beta1)) - ...
                A(2)*(HRF_time/T(2)).^alpha2.*exp((HRF_time-T(2))/(-beta2));
    
    HRF_gamma1 = HRF_gamma1(:);
    HRF_gamma2 = HRF_gamma2(:);
    HRF_gamma = HRF_gamma(:);
    
    % goodness of fit
    R2 = 1-sum((HRF-HRF_gamma).^2)/sum((HRF-mean(HRF)).^2); % R_square = 1 - variance of residual/variance of raw data

end

function err=impest_gamma(HRF,x,L,t)
    % impest_gamma: fit HRF using gamma kernel
    %
    % Gamma distribution kernel:
    % f(t,T,W,A) = A*(t/T).^alpha*exp((t-T)/(-beta))
    % where,
    % alpha = (T/W).^2*8.0*log(2.0)
    % beta = W^2(T*8.0*log(2.0))
    % so,
    % f(t,T,W,A) = A*(t/T).^((T/W).^2*8.0*log(2.0))*exp((t-T)/(-W^2(T*8.0*log(2.0))))
    %
    % see the following paper for more information:
    % Mariana M B Cardoso et al., The neuroimaging signal is a linear sum of
    % neurally distinct stimulus- and task-related components. Nature
    % Neuroscience, 2012.

    % Use two gamma function to fit
    A = x(1:2);
    T = x(3:4);
    W = x(5:6);
    
    alpha1 = (T(1)/W(1))^2*8.0*log(2.0);
    beta1 = W(1)^2/(T(1)*8.0*log(2.0));
    
    alpha2 = (T(2)/W(2))^2*8.0*log(2.0);
    beta2 = W(2)^2/(T(2)*8.0*log(2.0));

    HRF_gamma = A(1)*(t/T(1)).^alpha1.*exp((t-T(1))/(-beta1)) - ...
                A(2)*(t/T(2)).^alpha2.*exp((t-T(2))/(-beta2));


    HRF_gamma = HRF_gamma(:);  

    err=sum((HRF_gamma(1:L)-HRF(1:L)).^2);
end
