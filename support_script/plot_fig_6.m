function plot_fig_6(rootfolder)
%% subplot a
data_716_1 = load(fullfile(rootfolder,'Data','RBC_results','QZ0716','200303_007_LineScan.mat'));
data_716_2 = load(fullfile(rootfolder,'Data','RBC_results','QZ0716','200305_009_LineScan.mat'));

figure('name','Raw trace of RBCs using 2PLSM');
subplot(211);
imagesc(data_716_1.time, 1:121, data_716_1.image_filt_2D'); axis xy; colormap gray;
hold on;
plot(data_716_1.RBC_time, 60,'r.');
xlim([120, 130]);
xlabel('Time (s)');
title('Capillary #1, depth = 34\mum, diameter = 5 \mum');
subplot(212);
imagesc(data_716_2.time, 1:98, data_716_2.image_filt_2D'); axis xy; colormap gray;
hold on;
plot(data_716_2.RBC_time, 50,'r.');
xlim([40, 50]);
xlabel('Time (s)');
title('Capillary #2, depth = 15\mum, diameter = 5.3 \mum');
savefig(gcf, fullfile(rootfolder, 'Figure', 'Fig_6a'));
close(gcf);

%% subplot b
data = load(fullfile(rootfolder,'Data','RBC_results', 'data_pb.mat'));

% capillary #1
RBC_time = data.QZ0716.T200303_007.RBC_time;
idx = (RBC_time >= data.QZ0716.T200303_007.restSeg(1)) & (RBC_time <= data.QZ0716.T200303_007.restSeg(2));
RBC_time = RBC_time(idx);
RBC_time_diff = diff(RBC_time);

figure('name','RBC interval distribution and power spectrum for capillary #1');
subplot(121)
loglog(RBC_time_diff(1:end-1),RBC_time_diff(2:end),'color','k','Marker','.','LineStyle','none');
xlabel('RBC interval N (s)');
ylabel('RBC interval N+1 (s)');
axis square;
axis([0.001 10 0.001 10]);
subplot(122)
plot(data.QZ0716.T200303_007.RBC_plaw.frest, data.QZ0716.T200303_007.RBC_plaw.prest,'k');
hold on;
plot(data.QZ0716.T200303_007.RBC_plaw.plaw.rest.LSERout.freq_resamp,...
    10.^(data.QZ0716.T200303_007.RBC_plaw.plaw.rest.LSERout.slope*log10(data.QZ0716.T200303_007.RBC_plaw.plaw.rest.LSERout.freq_resamp)...
    +data.QZ0716.T200303_007.RBC_plaw.plaw.rest.LSERout.intercept),'r');
text(0.1, 0.15, ...
    ['log10(Power) = ', num2str(data.QZ0716.T200303_007.RBC_plaw.plaw.rest.LSERout.slope),...
    '* log10(f) + (', num2str(data.QZ0716.T200303_007.RBC_plaw.plaw.rest.LSERout.intercept),')'],'unit','normalized');
set(gca, 'xscale','log','yscale','log');
axis square;
xlabel('Frequency (Hz)');
ylabel('Power (s^2)');
savefig(gcf, fullfile(rootfolder, 'Figure', 'Fig_6b'));
close(gcf);


% Capillary #2
RBC_time = data.QZ0716.T200305_005.RBC_time;
idx = (RBC_time >= data.QZ0716.T200305_005.restSeg(1)) & (RBC_time <= data.QZ0716.T200305_005.restSeg(2));
RBC_time = RBC_time(idx);
RBC_time_diff = diff(RBC_time);

figure('name','RBC interval and power spectrum for capillary #2');
subplot(121)
loglog(RBC_time_diff(1:end-1),RBC_time_diff(2:end),'color','k','Marker','.','LineStyle','none');
xlabel('RBC interval N (s)');
ylabel('RBC interval N+1 (s)');
axis square;
axis([0.001 10 0.001 10]);
subplot(122)
plot(data.QZ0716.T200305_005.RBC_plaw.frest, data.QZ0716.T200305_005.RBC_plaw.prest,'k');
hold on;
plot(data.QZ0716.T200305_005.RBC_plaw.plaw.rest.LSERout.freq_resamp,...
    10.^(data.QZ0716.T200305_005.RBC_plaw.plaw.rest.LSERout.slope*log10(data.QZ0716.T200305_005.RBC_plaw.plaw.rest.LSERout.freq_resamp)...
    +data.QZ0716.T200305_005.RBC_plaw.plaw.rest.LSERout.intercept),'r');
text(0.1, 0.15, ...
    ['log10(Power) = ', num2str(data.QZ0716.T200305_005.RBC_plaw.plaw.rest.LSERout.slope),...
    '* log10(f) + (', num2str(data.QZ0716.T200305_005.RBC_plaw.plaw.rest.LSERout.intercept),')'],'unit','normalized');
set(gca, 'xscale','log','yscale','log');
axis square;
xlabel('Frequency (Hz)');
ylabel('Power (s^2)');
savefig(gcf, fullfile(rootfolder, 'Figure', 'Fig_6c'));
close(gcf);

%% fig 7d
p_rest = [];
stall = [];
slope = [];
animal_list = fieldnames(data);
% stall_events = 0;
% all_events = 0;
stall_interval = [];
all_interval = [];
trial_length = 0;
for animal_idx = 1:numel(animal_list)
    animal = animal_list{animal_idx};
    trial_list = fieldnames(data.(animal));
    for trial_idx = 1:numel(trial_list)
        trial = trial_list{trial_idx};
        p_rest = [p_rest;data.(animal).(trial).RBC_plaw.prest';];
        f_rest = data.(animal).(trial).RBC_plaw.frest;
        
        trial_length = trial_length + sum(diff(data.(animal).(trial).restSeg,[],2));
        tmp_RBC_interval = diff(data.(animal).(trial).RBC_time);
        stall_interval = [stall_interval;tmp_RBC_interval(tmp_RBC_interval>=1);];
        all_interval = [all_interval; tmp_RBC_interval;];
        stall = [stall; data.(animal).(trial).stall;];
        slope = [slope; data.(animal).(trial).RBC_plaw.plaw.rest.LSERout.exponent;];
    end
end

p_rest_stall = p_rest(find(stall),:);
p_rest_stall = p_rest_stall./sum(p_rest_stall,2);
p_rest_stall_ave = mean(p_rest_stall,1);
p_rest_stall_sem = std(p_rest_stall,[],1)/sqrt(size(p_rest_stall,1));

p_rest_nostall = p_rest(find(~stall),:);
p_rest_nostall = p_rest_nostall./sum(p_rest_nostall,2);
p_rest_nostall_ave = mean(p_rest_nostall,1);
p_rest_nostall_sem = std(p_rest_nostall,[],1)/sqrt(size(p_rest_nostall,1));


% median(stall_interval)
% iqr(stall_interval)
% 
% quantile(stall_interval, 0.975)
% quantile(stall_interval, 0.025)
% 
% median(all_interval)
% iqr(all_interval)
% 
% quantile(all_interval, 0.975)
% quantile(all_interval, 0.025)


figure('name','Power spectrum of RBC occurance with or without stall events');
subplot(121)
pt1 = box_line(f_rest,p_rest_nostall_ave,p_rest_nostall_sem, 'k'); hold on;
plot(f_rest,p_rest_nostall_ave,'k');
plot(f_rest, 10.^(mean(-slope(find(~stall)))*log10(f_rest)-2.5),'k--')
set(gca, 'xscale','log', 'yscale','log');
axis square;
pt2 = box_line(f_rest,p_rest_stall_ave,p_rest_stall_sem, 'r'); hold on;
plot(f_rest,p_rest_stall_ave,'r');
plot(f_rest, 10.^(mean(-slope(find(stall)))*log10(f_rest)-3),'r--')
set(gca, 'xscale','log', 'yscale','log');
axis([0.01 1 1e-3 10^(-0.5)]);
axis square;
ylabel('Power (s^2/Hz)');
xlabel('Frequency (Hz)');
subplot(122)
plot(1,slope(find(~stall)),...
    'linestyle','none',...
    'marker','o',...
    'markersize',10,...
    'markerfacecolor',[0.5 0.5 0.5],...
    'markeredgecolor','k',...
    'linewidth',2);
hold on;
plot(1,slope(find(stall)),...
    'linestyle','none',...
    'marker','o',...
    'markersize',10,...
    'markerfacecolor',[0.5 0.5 0.5],...
    'markeredgecolor',[0.91 0.41 0.17],... % orange color
    'linewidth',2);
ylabel('Slope');
ylim([0 1.5])
set(gca, 'xtick',[], 'xticklabel',[]);
savefig(gcf, fullfile(rootfolder, 'Figure', 'Fig_6d'));
close(gcf);
%% observed RBC interval Poincare plot
figure('name','Observed RBC interval for all the data');
animal_list = fieldnames(data);
all_RBC_interval = [];
for animal_idx = 1:numel(animal_list)
    animal = animal_list{animal_idx};
    trial_list = fieldnames(data.(animal));
    for trial_idx = 1:numel(trial_list)
        trial = trial_list{trial_idx};
        RBC_time = data.(animal).(trial).RBC_time;
        idx = (RBC_time >= data.(animal).(trial).restSeg(1)) & (RBC_time <= data.(animal).(trial).restSeg(2));
        RBC_time = RBC_time(idx);
        RBC_time_diff = diff(RBC_time);
        all_RBC_interval = [all_RBC_interval; RBC_time_diff;];        
        if ~(strcmp(animal,'QZ0720') & strcmp(trial, 'T200304_009')) &&...
                ~(strcmp(animal,'QZ0724') & strcmp(trial, 'T201202_005'))
            loglog(RBC_time_diff(1:end-1),RBC_time_diff(2:end),'color','k','Marker','.','LineStyle','none'); 
        end
        hold on;
    end
end
hold off;
xlabel('RBC interval N (s)');
ylabel('RBC interval N+1 (s)');
axis square;
axis([0.001 30 0.001 30]);
savefig(gcf, fullfile(rootfolder, 'Figure', 'Fig_6g_right'));
close(gcf);

%% figure 7f and g
data_model_60_80 = load(fullfile(rootfolder,'Data','RBC_results','Model_60_80_cps.mat')); % 60-80 cells per second


figure('name','Model RBC passage and oxygenation');
h1 = subplot(311);
plot(data_model_60_80.time_pseudo/1000, 10.^data_model_60_80.data_pseudo,'k');
ylabel('RBC interval (s)');
h2 = subplot(312);
plot(data_model_60_80.PO2_time, data_model_60_80.PO2,'k');
hold on;
plot(data_model_60_80.PO2_time, smoothdata(data_model_60_80.PO2,'gauss',0.2*1000),'r');
ylabel('PcO2 (mmHg)');
h3 = subplot(313);
imagesc((0:size(data_model_60_80.PtO2,1)-1)/1000,3:20,data_model_60_80.PtO2');
axis xy;
set(gca, 'ydir','reverse')
ylabel('Distance (\mum)');
xlabel('Time (s)')
linkaxes([h1,h2,h3],'x')
savefig(gcf, fullfile(rootfolder, 'Figure', 'Fig_6f'));
close(gcf);

figure('name','RBC interval distribution'); 
loglog(10.^data_model_60_80.data_pseudo(1:end-1), 10.^data_model_60_80.data_pseudo(2:end),'k.');
xlabel('RBC interval N (s)');
ylabel('RBC interval N+1 (s)');
axis square;
axis([10^(-3) 30 10^(-3) 30]);
savefig(gcf, fullfile(rootfolder, 'Figure', 'Fig_6g_left'));
close(gcf);

%% figure 7h
observedData = load(fullfile(rootfolder,'Data','RBC_results','observedData.mat')); observedData = observedData.out;
simulatedData = load(fullfile(rootfolder,'Data','RBC_results','simulatedData.mat')); simulatedData = simulatedData.data;


monitorPos = get(0,'MonitorPositions'); % [x y w ht] X # of monitors
w = 1200; ht = 600; % Define window size: width(w) and height(ht)
mon = 1; % Only one monitor

% Determine positioning for main figure
pos = [((monitorPos(mon,3)-w)/2)+monitorPos(mon,1)/2,...
    ((monitorPos(mon,4)-ht)/2)+monitorPos(mon,2)/2,...
    w ht];

figure('position',pos,'color','w',...
    'name','Compare the model vs. observed data');
h11 = axes('units','normalized','position',[0.05 0.10 0.4 0.8]);
hold on;
plot(observedData.freq, quantile(observedData.power_all,0.75),'color','g');
plot(observedData.freq, quantile(observedData.power_all,0.25),'color','k');
plot(observedData.freq, median(observedData.power_all,'omitnan'),'color','m')
plot(simulatedData.freq, simulatedData.power_CI95,'r');
plot(simulatedData.freq, mean(simulatedData.power),'c');
set(gca,'box','off',...
    'xscale','log','yscale','log',...
    'xlim',[0.01 1],...
    'xticklabel',{'0.01','0.1', '1'});
ylabel('Power (mmHg^2/Hz)');
legend({'75% quantile','25% quantile','Median','95% CI UB','95% CI LB','Mean simulated power'});
title('Oxygen power spectrum: rest');

h12 = axes('units','normalized',...
    'position',[0.05 0.10-0.16/2 0.4 0.16],...
    'box','off'); % plot residual
hold on;
ylim([-0.5 0.5]);
plot(observedData.freq, log10(median(observedData.power_all,'omitnan'))-log10(mean(simulatedData.power)),'color','m');
set(gca,'xscale','log',...
    'yaxislocation','right',...
    'xtick',[],...
    'color','none',...
    'xcolor','w');
xlabel('Frequency (Hz)','color','k');
ylabel('log10(Power)'); % wait for power spectrum during rest and running
savefig(gcf, fullfile(rootfolder, 'Figure', 'Fig_6h'));
close(gcf);


end