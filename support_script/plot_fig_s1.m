function plot_fig_s1(rootfolder)
% Simulate time series of fractal, white noise and periodic noise
% Author: Qingguang Zhang (qingguang.zhang@gmail.com)
% see also:
%       Chronux Toolbox
%       ffgn.m

%% fractional Gaussian noise
% generate fractional Gaussian noise using circulant embedding method
H = 0.9; % Hurst exponent
L = 18000; % Length of the dataset, samples
SD = 0.1; % standard deviation
n = 1; % one dimension signal

FGM = ffgn(SD,H,n,L,0); % generate fractional Gaussian noise
Fs = 30; % sample rate, 30 Hz
time = ((1:length(FGM))-1)/Fs; % time stamps

% Using multi-taper analysis to get power spectrum of FGM
params.Fs = Fs; % sampling frequency
params.fpass = [0 1]; % band pass filter
params.tapers = [1 1]; % [time band width, number of tapers]
win = 60; % window size, in seconds

% calculate power spectrum using multi-taper analysis
[p_FGM,f_FGM] = mtspectrumsegc(detrend(FGM),win,params); 

% fit the power spectrum using linear regression
f_FGM = f_FGM(2:end); % ignore the first element, since it is zero
p_FGM = p_FGM(2:end);

% NOTE: exponent = 2*H-1 for fractional gaussian noise
FGM_plaw = PowerLawFit(f_FGM,p_FGM);

% DFA analysis
FGM_DFA = DFA(FGM,2,[1,length(FGM)]);
%% White noise
% generate white nosie time series
White_noise = SD*randn(L,1);

% calculate power spectrum using multi-taper analysis
[p_WN,f_WN] = mtspectrumsegc(detrend(White_noise),win,params);

% fit the power spectrum using linear regression
f_WN = f_WN(2:end);
p_WN = p_WN(2:end);
WN_plaw = PowerLawFit(f_WN,p_WN);

% DFA analysis
WN_DFA = DFA(White_noise,2,[1,length(White_noise)]);
%% Periodic noise
Gaussian_noise = randn(1,L); % Gaussian noise with unity variance

[zeroa, poleb, gain] = butter(3,2/Fs*[0.1,2],'bandpass');
[sos,g] = zp2sos(zeroa,poleb, gain);

Gaussian_noise = filtfilt(sos,g,Gaussian_noise);

Periodic_noise = 0.1*(1+Gaussian_noise).*cos(2*pi*0.2.*time);

[p_PN,f_PN] = mtspectrumsegc(detrend(Periodic_noise),win,params); % segmented data

f_PN = f_PN(2:end);
p_PN = p_PN(2:end);
PN_plaw = PowerLawFit(f_PN,p_PN);

% DFA analysis
PN_DFA = DFA(Periodic_noise,2,[1,length(Periodic_noise)]);
%% fractal + periodic
Pseudo_data = FGM+Periodic_noise;
[p_Pseudo,f_Pseudo] = mtspectrumsegc(detrend(Pseudo_data),win,params); % segmented data

f_Pseudo = f_Pseudo(2:end);
p_Pseudo = p_Pseudo(2:end);
Pseudo_plaw = PowerLawFit(f_Pseudo,p_Pseudo);

% DFA analysis
Pseudo_DFA = DFA(Pseudo_data,2,[1,length(Pseudo_data)]);
%% generate figure
xlimit = [20 80];
figure('name','Time series and power spectrum');
h1 = subplot(431);
plot(time,White_noise); xlabel('Time (s)'); ylabel('White noise');
xlim(xlimit);

h2 = subplot(432);
plot(f_WN, p_WN,'k'); hold on;
plot(WN_plaw.LSERout.freq_resamp, 10.^(WN_plaw.LSERout.predict),'r');
set(gca,'box','off','xscale','log','yscale','log',...
    'xlim',[0 1],'xticklabel',{'0.1', '1'},...
    'ylim', [10^-4.3, 10^-0.8]);
xlabel('Frequency (Hz)'); ylabel('Power (1/Hz)');
text(0.1,0.4,...
    ['Power','\propto' 'f^{0}'], 'unit','normalized','color','r');

h3 = subplot(433);
plot(WN_DFA.timeScale/Fs, WN_DFA.Fn,'ko'); hold on;
plot(WN_DFA.timeScale/Fs, exp(WN_DFA.logfit),'r');
set(gca,'box','off','xscale','log','yscale','log');
axis([1 60 0.05 7]);
xlabel('Time Scale (s)'); ylabel('F(n)');
text(0.1,0.7,...
    ['\alpha = ', num2str(WN_DFA.coefficient(1))], 'unit','normalized','color','r');

h4 = subplot(434);
plot(time,Periodic_noise); xlabel('Time (s)'); ylabel('Periodic noise');
xlim(xlimit);

h5 = subplot(435);
plot(f_PN, p_PN,'k'); hold on;
plot(PN_plaw.LSERout.freq_resamp, 10.^(PN_plaw.LSERout.predict),'r');
set(gca,'box','off', 'xscale','log','yscale','log',...
    'xlim',[0 1],'xticklabel',{'0.1', '1'},...
    'ylim', [10^-4.3, 10^-0.8]);
xlabel('Frequency (Hz)'); ylabel('Power (1/Hz)');
text(0.1,0.4,...
    ['Power','\propto' 'f^{0}'], 'unit','normalized','color','r');

h6 = subplot(436);
plot(PN_DFA.timeScale/Fs, PN_DFA.Fn,'ko'); hold on;
set(gca,'box','off','xscale','log','yscale','log');
axis([1 60 0.05 7]);
xlabel('Time Scale (s)'); ylabel('F(n)');

h7 = subplot(437);
plot(time,FGM); xlabel('Time (s)'); ylabel('Fractal noise');
xlim(xlimit);

h8 = subplot(438);
plot(f_FGM, p_FGM,'k'); hold on;
plot(FGM_plaw.LSERout.freq_resamp, 10.^(FGM_plaw.LSERout.predict),'r');
set(gca,'box','off', 'xscale','log','yscale','log',...
    'xlim',[0 1],'xticklabel',{'0.1', '1'},...
    'ylim', [10^-4.3, 10^-0.8]);
xlabel('Frequency (Hz)'); ylabel('Power (1/Hz)');
text(0.1,0.15,...
    ['Power','\propto' 'f^{-0.9}'], 'unit','normalized','color','r');

h9 = subplot(439);
plot(FGM_DFA.timeScale/Fs, FGM_DFA.Fn,'ko'); hold on;
plot(FGM_DFA.timeScale/Fs, exp(FGM_DFA.logfit),'r');
set(gca,'box','off','xscale','log','yscale','log');
axis([1 60 0.05 7]);
xlabel('Time Scale (s)'); ylabel('F(n)');
text(0.1,0.7,...
    ['\alpha = ', num2str(FGM_DFA.coefficient(1))], 'unit','normalized','color','r');


h10 = subplot(4,3,10);
plot(time,Pseudo_data); xlabel('Time (s)'); ylabel('Fractal+Periodic noise');
xlim(xlimit);

h11 = subplot(4,3,11);
plot(f_Pseudo, p_Pseudo,'k'); hold on;
plot(Pseudo_plaw.LSERout.freq_resamp, 10.^(Pseudo_plaw.LSERout.predict),'r');
set(gca,'box','off', 'xscale','log','yscale','log',...
    'xlim',[0 1],'xticklabel',{'0.1', '1'},...
    'ylim', [10^-4.3, 10^-0.8]);
xlabel('Frequency (Hz)'); ylabel('Power (1/Hz)');
text(0.1,0.15,...
    ['Power','\propto' 'f^{-0.8}'], 'unit','normalized','color','r');

h12 = subplot(4,3,12);
plot(Pseudo_DFA.timeScale/Fs, Pseudo_DFA.Fn,'ko'); hold on;
plot(Pseudo_DFA.timeScale/Fs, exp(Pseudo_DFA.logfit),'r');
set(gca,'box','off','xscale','log','yscale','log');
axis([1 60 0.05 7]);
xlabel('Time Scale (s)'); ylabel('F(n)');
text(0.1,0.7,...
    ['\alpha = ', num2str(Pseudo_DFA.coefficient(1))], 'unit','normalized','color','r');


linkaxes([h1,h4,h7,h10],'x');

savefig(gcf, fullfile(rootfolder, 'Figure', 'Fig_S1'));
close(gcf);
end