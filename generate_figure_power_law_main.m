function generate_figure_power_law_main()
close all; clear all; clc;
currentFile = mfilename('fullpath');
fileparts = strsplit(currentFile, filesep);
if ismac
    currentFolder = fullfile(filesep, fileparts{1:end-1});
else
    currentFolder = fullfile(fileparts{1:end-1});
end

addpath(genpath(currentFolder));
fileparts = strsplit(currentFolder, filesep);
if ismac
    rootfolder = fullfile(filesep, fileparts{1:end-1});
    figurefolder = fullfile(filesep, fileparts{1:end-1},'Figure');
else
    rootfolder = fullfile(fileparts{1:end-1});
    figurefolder = fullfile(fileparts{1:end-1},'Figure');
end

% generate a figure folder to save all generated figures
if ~exist(figurefolder, 'dir'), mkdir(figurefolder); end

%% figure 1 and figure 2c
plot_fig_1bcd_2c(rootfolder);
plot_fig_1efg(rootfolder);

%% figure S1
plot_fig_s1(rootfolder);

%% figure 2d, S2, and S3
plot_fig_2d_S2_S3(rootfolder);

%% figure 3
plot_fig_3(rootfolder);

%% figure 4
plot_fig_4(rootfolder);

%% figure 5, S5 and S6
plot_fig_5b(rootfolder);
plot_fig_5c(rootfolder);
plot_fig_5def_S5_S6(rootfolder);

%% figure 6
plot_fig_6(rootfolder);

disp('Generating figure finished');

end
