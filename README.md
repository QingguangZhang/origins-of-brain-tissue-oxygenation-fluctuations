This document contains the Matlab code used to generate the figures for the article "Origins of 1/f-like tissue oxygenation fluctuations in the murine cortex" by Qingguang Zhang, Kyle Gheres and Patrick Drew (10.1371/journal.pbio.3001298).

INSTRUCTIONS FOR DOWNLOADING DATA AND CODE
--------------------------------------
Download this code repository and the dataset from the following locations:

- Code repository: https://gitlab.com/QingguangZhang/origins-of-brain-tissue-oxygenation-fluctuations

- Data respository: https://doi.org/10.5061/dryad.pg4f4qrmt 

Download and extract the .zip files titled: "Zhang_PLoS_Biology_2021.zip". Add the extracted code folder to the folder "Zhang_PLoS_Biology_2021".


INSTRUCTIONS FOR GENERATING FIGURES
--------------------------------------
Find the MATLAB script titled "generate_figure_power_law_main.m"

Run this script and all the figures will be saved as *.fig file in the folder named "Figure" under the main folder "Zhang_PLoS_Biology_2021"

*** An important note: Subplots of some figures are populated between analysis. Clicking on figure windows while the figures are generating may scramble the contents of the figures and give unexpected results. Therefore, it is recommended that you wait until the code finishes running before interacting with the figures.

*** This code has been tested on Windows 7/10 operating systems and Mac Mojave using MATLAB versions 2015b and 2019b. Compatibility with other operating systems or future MATLAB versions has not been verified. No other non-standard hardware is required.

EXPECTED RUN TIME
-------------------------------------
Generate figure code: this code takes about 10 minutes to finish.

REFERENCES
-------------------------------------

- cbrewer toolbox by Charles Robert https://www.mathworks.com/matlabcentral/fileexchange/34087-cbrewer-colorbrewer-schemes-for-matlab

- Chronux Toolbox http://chronux.org/

- ffgn.m by Yingchun Zhou and Stilian Stoev https://www.mathworks.com/matlabcentral/mlc-downloads/downloads/submissions/23554/versions/6/previews/Matlab/03LongMemory/Theory/ffgn.m/index.html

- fminsearchbnd.m by John D'Errico https://www.mathworks.com/matlabcentral/fileexchange/8277-fminsearchbnd-fminsearchcon

- plotSpread toolbox by Jonas https://www.mathworks.com/matlabcentral/fileexchange/37105-plot-spread-points-beeswarm-plot
- shadedErrorBar.m by Rob Campbell https://www.mathworks.com/matlabcentral/fileexchange/26311-raacampbell-shadederrorbar
